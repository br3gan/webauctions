/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatabaseUtils;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
 
import Entities.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
 
public class DatabaseAccessUtils {

  public static int getLastFieldInt(Connection conn, String field, String table) throws SQLException{
      String sql = "SELECT "+field+" FROM "+table+" ORDER BY "+field+" DESC LIMIT 1";
      
      PreparedStatement pstm = conn.prepareStatement(sql);
      ResultSet rs = pstm.executeQuery();
      if (rs.next()) {
          return rs.getInt(field);
      }
      return -1;
  }  
  
  public static String getLastFieldStr(Connection conn, String field, String table) throws SQLException{
      String sql = "SELECT "+field+" FROM "+table+" ORDER BY "+field+" DESC LIMIT 1";
      
      PreparedStatement pstm = conn.prepareStatement(sql);
      ResultSet rs = pstm.executeQuery();
      if (rs.next()) {
          return rs.getString(field);
      }
      return null;
  } 
    
  public static User findUser(Connection conn, String Username, String Password) throws SQLException {
                   
      String sql = "SELECT * FROM User u " + "WHERE u.Username = ? AND u.Password = ?";
 
      PreparedStatement pstm = conn.prepareStatement(sql);
      pstm.setString(1, Username);
      pstm.setString(2, Password);
      ResultSet rs = pstm.executeQuery();
 
      
      if (rs.next()) {
          int userID = rs.getInt("ID");
          String firstName = rs.getString("Firstname");
          String lastName = rs.getString("Lastname");
          String mail = rs.getString("Mail");
          String telephone = rs.getString("Telephone");
          int locationID = rs.getInt("LocationID");
          String ssn = rs.getString("SSN");
          
          User user = new User(userID, Username, Password, firstName, lastName, mail, telephone, locationID, ssn);
          
          return user;
      }
      return null;
  }
  
  public static User findUser(Connection conn, String uName) throws SQLException {
                   
      String sql = "SELECT * FROM User u " + "WHERE u.Username = ?";
 
      PreparedStatement pstm = conn.prepareStatement(sql);
      pstm.setString(1, uName);
      ResultSet rs = pstm.executeQuery();
      
      if (rs.next()) {
          
          int userID = rs.getInt("ID");
          String Username = rs.getString("Username");
          String Password = rs.getString("Password");
          String firstName = rs.getString("Firstname");
          String lastName = rs.getString("Lastname");
          String mail = rs.getString("Mail");
          String telephone = rs.getString("Telephone");
          int locationID = rs.getInt("LocationID");
          String ssn = rs.getString("SSN");
          
          User user = new User(userID, Username, Password, firstName, lastName, mail, telephone, locationID, ssn);
          
          return user;
      }
      return null;
  }
  
  public static User findUser(Connection conn, int ID) throws SQLException {
      String sql = "SELECT * FROM User u " + "WHERE u.ID = ?";
 
      PreparedStatement pstm = conn.prepareStatement(sql);
      pstm.setInt(1, ID);
      ResultSet rs = pstm.executeQuery();
      
      if (rs.next()) {
          
          int userID = rs.getInt("ID");
          String Username = rs.getString("Username");
          String Password = rs.getString("Password");
          String firstName = rs.getString("Firstname");
          String lastName = rs.getString("Lastname");
          String mail = rs.getString("Mail");
          String telephone = rs.getString("Telephone");
          int locationID = rs.getInt("LocationID");
          String ssn = rs.getString("SSN");
          
          User user = new User(userID, Username, Password, firstName, lastName, mail, telephone, locationID, ssn);
          
          return user;
      }
      return null;
  }
  
  public static UnconfirmedUser findUnconfirmedUser(Connection conn, int ID) throws SQLException {
                   
      String sql = "SELECT * FROM UnconfirmedUser u " + "WHERE u.ID = ?";
 
      PreparedStatement pstm = conn.prepareStatement(sql);
      pstm.setInt(1, ID);
      ResultSet rs = pstm.executeQuery();
      
      if (rs.next()) {
          
          int userID = rs.getInt("ID");
          String Username = rs.getString("Username");
          String Password = rs.getString("Password");
          String firstName = rs.getString("Firstname");
          String lastName = rs.getString("Lastname");
          String mail = rs.getString("Mail");
          String telephone = rs.getString("Telephone");
          int locationID = rs.getInt("LocationID");
          String ssn = rs.getString("SSN");
          
          UnconfirmedUser user = new UnconfirmedUser(userID, Username, Password, firstName, lastName, mail, telephone, locationID, ssn);
          
          return user;
      }
      return null;
  }

  
    public static ArrayList<User> findUsers(Connection conn) throws SQLException{
        String sql = "Select * FROM User";

        PreparedStatement pstm = conn.prepareCall(sql);
        ResultSet rs = pstm.executeQuery();
        ArrayList<User> Users = new ArrayList<User>();
        
        while (rs.next()) {
            int ID = rs.getInt("ID");
            String Username = rs.getString("Username");
            String Password = rs.getString("Password");
            String Firstname = rs.getString("Firstname");
            String Lastname = rs.getString("Lastname");
            String Mail = rs.getString("Mail"); 
            String Telephone = rs.getString("Telephone"); 
            int LocationID = rs.getInt("LocationID");
            String SSN = rs.getString("SSN");
            
            User user = new User(ID, Username, Password, Firstname, Lastname, Mail, Telephone, LocationID, SSN);
            Users.add(user);
            
        }
          System.out.println("Returning "+ Users.size() +" item(s)");
          return Users;
    }
    
    public static ArrayList<UnconfirmedUser> findUnconfirmedUsers(Connection conn) throws SQLException{
        String sql = "Select * FROM UnconfirmedUser";

        PreparedStatement pstm = conn.prepareCall(sql);
        ResultSet rs = pstm.executeQuery();
        ArrayList<UnconfirmedUser> UnconfirmedUser = new ArrayList<UnconfirmedUser>();

        while (rs.next()) {
            int ID = rs.getInt("ID");
            String Username = rs.getString("Username");
            String Password = rs.getString("Password");
            String Firstname = rs.getString("Firstname");
            String Lastname = rs.getString("Lastname");
            String Mail = rs.getString("Mail"); 
            String Telephone = rs.getString("Telephone"); 
            int LocationID = rs.getInt("LocationID");
            String SSN = rs.getString("SSN");

            UnconfirmedUser user = new UnconfirmedUser(ID, Username, Password, Firstname, Lastname, Mail, Telephone, LocationID, SSN);
            UnconfirmedUser.add(user);

        }
        System.out.println("[Unconfirmed] returning "+ UnconfirmedUser.size() +" item(s)");
        return UnconfirmedUser;
    }
    
public static Admin findAdmin(Connection conn, String Username, String Password) throws SQLException {
                   
    String sql = "SELECT * FROM Admin a " + "WHERE a.Username = ? AND a.Password= ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, Username);
        pstm.setString(2, Password);
        ResultSet rs = pstm.executeQuery();

        if (rs.next()) {
            int userID = rs.getInt("ID");
            String firstName = rs.getString("Firstname");
            String lastName = rs.getString("Lastname");
            String mail = rs.getString("Mail");
            String telephone = rs.getString("Telephone");
            String ssn = rs.getString("SSN");

            Admin admin = new Admin(userID, Username, Password, firstName, lastName, mail, telephone, ssn);

            return admin;
        }
        return null;
}

    
public static int findItemID(Connection conn, String Name) throws SQLException{

    String sql = "SELECT ID FROM Item i " + "WHERE i.Name = ?";
 
    PreparedStatement pstm = conn.prepareStatement(sql);
    pstm.setString(1, Name);
    ResultSet rs = pstm.executeQuery();
      
    if (rs.next()) {
        return rs.getInt("ID");
    }
    return -1;

}


  public static UnconfirmedUser findUnconfirmedUser(Connection conn, String uName) throws SQLException {
                   
      String sql = "SELECT * FROM UnconfirmedUser u " + "WHERE u.Username = ?";
 
      PreparedStatement pstm = conn.prepareStatement(sql);
      pstm.setString(1, uName);
      ResultSet rs = pstm.executeQuery();
      
      if (rs.next()) {
          
          int userID = rs.getInt("ID");
          String Username = rs.getString("Username");
          String password = rs.getString("Password");
          String firstName = rs.getString("Firstname");
          String lastName = rs.getString("Lastname");
          String mail = rs.getString("Mail");
          String telephone = rs.getString("Telephone");
          int locationID = rs.getInt("LocationID");
          String ssn = rs.getString("SSN");
          
          UnconfirmedUser user = new UnconfirmedUser(userID, Username, password, firstName, lastName, mail, telephone, locationID, ssn);
          
          return user;
      }
      return null;
  }
  
  
    public static String findSellerUsername(Connection conn, int SellerID) throws SQLException{
    
        String sql = "SELECT Username FROM User WHERE ID = ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, SellerID);
        ResultSet rs = pstm.executeQuery();

        if (rs.next())
            return rs.getString("Username");
        
        return null;
    }
  
  public static void insertUser(Connection conn, User user) throws SQLException {
      String sql = "Insert into User(ID, Username, Password, Firstname, Lastname, Mail, Telephone, LocationID, SSN) values (?,?,?,?,?,?,?,?,?)";
      PreparedStatement pstm = conn.prepareStatement(sql);
      
      pstm.setInt(1, user.getId());
      pstm.setString(2, user.getUsername());
      pstm.setString(3, user.getPassword());
      pstm.setString(4, user.getFirstname());
      pstm.setString(5, user.getLastname());
      pstm.setString(6, user.getMail());
      pstm.setString(7, user.getTelephone());
      pstm.setInt(8, user.getLocationID());
      pstm.setString(9, user.getSsn());
           
      pstm.executeUpdate();
  }
  
  public static void insertUnconfirmedUser(Connection conn, UnconfirmedUser user) throws SQLException {
      String sql = "Insert into UnconfirmedUser(ID, Username, Password, Firstname, Lastname, Mail, Telephone, LocationID, SSN) values (?,?,?,?,?,?,?,?,?)";
 
      PreparedStatement pstm = conn.prepareStatement(sql);
      
      pstm.setInt(1, user.getId());
      pstm.setString(2, user.getUsername());
      pstm.setString(3, user.getPassword());
      pstm.setString(4, user.getFirstname());
      pstm.setString(5, user.getLastname());
      pstm.setString(6, user.getMail());
      pstm.setString(7, user.getTelephone());
      pstm.setInt(8, user.getLocationID());
      pstm.setString(9, user.getSsn());
                 
      pstm.executeUpdate();
  }
  
  public static void insertItem(Connection conn, Item item) throws SQLException{
      
//      int ID, String Name, float CurrPrice, float BuyPrice, float FirstBid, int NumberOfBids, int BidsID, String Address, String Country, Date Start, Date End, int SellerID, String ItemDescription
    String sql = "Insert into Item(ID, Name, CurrPrice, BuyPrice, FirstBid, NumberOfBids, BidsID, LocationID, Start, End, SellerID, ItemDescription) values (?,?,?,?,?,?,?,?,?,?,?,?)";

    PreparedStatement pstm = conn.prepareStatement(sql);
    pstm.setInt(1, item.getId());
    pstm.setString(2, item.getName());
    pstm.setFloat(3, item.getCurrPrice());
    pstm.setFloat(4, item.getBuyPrice());
    pstm.setFloat(5, item.getCurrPrice());
    pstm.setInt(6, item.getNumberOfBids());
    pstm.setNull(7, java.sql.Types.INTEGER);
    pstm.setInt(8, item.getLocationID());
    SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd hh:mm:ss");
    pstm.setString(9, ft.format(item.getStart()));
    pstm.setString(10, ft.format(item.getEnd()));
    pstm.setInt(11, item.getSellerID());
    pstm.setString(12, item.getItemDescription());

    pstm.executeUpdate();
      
  }
  
    public static Item findItem(Connection conn, int ItemID) throws SQLException {
    
        String sql = "SELECT * FROM Item WHERE ID = ?";
        
        PreparedStatement pstm = conn.prepareCall(sql);
        pstm.setInt(1, ItemID);
        ResultSet rs = pstm.executeQuery();
        Item item = null;

        if (rs.next()) {
            int ID = rs.getInt("ID");
            String Name = rs.getString("Name");
            float CurrPrice = rs.getFloat("CurrPrice");
            float BuyPrice = rs.getFloat("BuyPrice");
            String StrFirstBid = rs.getString("FirstBid");
            float FirstBid=-1;
            if (StrFirstBid==null){
                FirstBid = CurrPrice;
            }
            else{
                FirstBid = Integer.parseInt(StrFirstBid);
            }
            String StrNumberOfBids = rs.getString("NumberOfBids"); 
            int NumberOfBids = Integer.parseInt(StrNumberOfBids);
            String StrBidsID = rs.getString("BidsID");
            int BidsID=-1;
            if (StrBidsID==null){
                BidsID = 0;
            }
            else{
                BidsID = Integer.parseInt(StrBidsID);
            }
            int LocationID = rs.getInt("LocationID");
            java.util.Date Start = rs.getDate("Start");
            java.util.Date End = rs.getDate("End");
            int SellerID = rs.getInt("SellerID");
            String ItemDescription = rs.getString("ItemDescription");
            item = new Item(ID, Name, CurrPrice, BuyPrice, FirstBid, NumberOfBids, BidsID, LocationID, Start, End, SellerID, ItemDescription);
            
            return item;
        }
    
        return null;
    }
    
  public static ArrayList<Item> findItems(Connection conn) throws SQLException{
        String sql = "Select * FROM Item";
      
        PreparedStatement pstm = conn.prepareCall(sql);
        ResultSet rs = pstm.executeQuery();
        ArrayList<Item> Items = new ArrayList<Item>();
        while (rs.next()) {
            int ID = rs.getInt("ID");
            String Name = rs.getString("Name");
            float CurrPrice = rs.getFloat("CurrPrice");
            float BuyPrice = rs.getFloat("BuyPrice");
            String StrFirstBid = rs.getString("FirstBid");
            float FirstBid=-1;
            if (StrFirstBid==null){
                FirstBid = CurrPrice;
            }
            else{
                FirstBid = Integer.parseInt(StrFirstBid);
            }
            String StrNumberOfBids = rs.getString("NumberOfBids"); 
            int NumberOfBids = Integer.parseInt(StrNumberOfBids);
            String StrBidsID = rs.getString("BidsID");
            int BidsID=-1;
            if (StrBidsID==null){
                BidsID = 0;
            }
            else{
                BidsID = Integer.parseInt(StrBidsID);
            }
            int LocationID = rs.getInt("LocationID");
            java.util.Date Start = rs.getDate("Start");
            java.util.Date End = rs.getDate("End");
            int SellerID = rs.getInt("SellerID");
            String ItemDescription = rs.getString("ItemDescription");
            Item item = new Item(ID, Name, CurrPrice, BuyPrice, FirstBid, NumberOfBids, BidsID, LocationID, Start, End, SellerID, ItemDescription);
            
            Items.add(item);
        }
        
        System.out.println("Returning "+ Items.size() +" item(s)");

        return Items;
  }
  
  

    public static ArrayList<Item> findItems(Connection conn, int UserID) throws SQLException{
        
        String sql = "Select * FROM Item WHERE SellerID =?";
      
        PreparedStatement pstm = conn.prepareCall(sql);
        pstm.setInt(1, UserID);
        ResultSet rs = pstm.executeQuery();
        
        ArrayList<Item> Items = new ArrayList<Item>();
        while (rs.next()) {
            int ID = rs.getInt("ID");
            String Name = rs.getString("Name");
            float CurrPrice = rs.getFloat("CurrPrice");
            float BuyPrice = rs.getFloat("BuyPrice");
            String StrFirstBid = rs.getString("FirstBid");
            float FirstBid=-1;
            if (StrFirstBid==null){
                FirstBid = CurrPrice;
            }
            else{
                FirstBid = Integer.parseInt(StrFirstBid);
            }
            String StrNumberOfBids = rs.getString("NumberOfBids"); 
            int NumberOfBids = Integer.parseInt(StrNumberOfBids);
            String StrBidsID = rs.getString("BidsID");
            int BidsID=-1;
            if (StrBidsID==null){
                BidsID = 0;
            }
            else{
                BidsID = Integer.parseInt(StrBidsID);
            }
            int LocationID = rs.getInt("LocationID");
            java.util.Date Start = rs.getDate("Start");
            java.util.Date End = rs.getDate("End");
            int SellerID = rs.getInt("SellerID");
            String ItemDescription = rs.getString("ItemDescription");
            Item item = new Item(ID, Name, CurrPrice, BuyPrice, FirstBid, NumberOfBids, BidsID, LocationID, Start, End, SellerID, ItemDescription);
            
            Items.add(item);
        }
        
        System.out.println("Returning "+ Items.size() +" item(s)");

        return Items;
        
    }

  
  public static int findLocation(Connection conn, String address, String country, String postcode) throws SQLException {
                   
      String sql = "SELECT * FROM Location l WHERE l.Address = ? AND l.Country = ? AND l.Postcode = ? ";
 
      PreparedStatement pstm = conn.prepareStatement(sql);
      pstm.setString(1, address);
      pstm.setString(2, country);
      pstm.setString(3, postcode);
      ResultSet rs = pstm.executeQuery();
      
      if (rs.next()) {
          return rs.getInt("ID");
      }
      return -1;
  }
  
  public static void insertLocation(Connection conn, Location location) throws SQLException {
      String sql = "Insert into Location(ID, Address, Country, PostCode) values (?,?,?,?)";
 
      PreparedStatement pstm = conn.prepareStatement(sql);
   
      pstm.setInt(1, location.getId());
      pstm.setString(2, location.getAddress());
      pstm.setString(3, location.getCountry());
      pstm.setString(4, location.getPostCode());
      
      pstm.executeUpdate();
  }
  
    public static Location findLocationFromID(Connection conn, int locID) throws SQLException {

        String sql = "SELECT * FROM Location WHERE Location.ID = ?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, locID);
        ResultSet rs = pstm.executeQuery();

        if (rs.next()) {
            int ID = rs.getInt("ID");
            String Address = rs.getString("Address");
            String Country= rs.getString("Country");
            String PostCode = rs.getString("PostCode");
            
            Location location = new Location(ID, Address, Country, PostCode);
            location.printLocation();
            return location;
        }

        return null;
    }
  
    public static void insertCaregories(Connection conn, int ItemID, ArrayList<String> Categories) throws SQLException{
        String sql = "Insert into Category(ItemID, Kind) values (?,?)";
        String sql2 = "Select * from Category WHERE ItemID=? AND Kind=?";
        
        for(String l : Categories) {
            
            PreparedStatement pstm2 = conn.prepareStatement(sql2);
            pstm2.setInt(1, ItemID);
            pstm2.setString(2, l);    
            ResultSet rs2 = pstm2.executeQuery();
            if (rs2.next()){
                continue;
            }
            PreparedStatement pstm = conn.prepareStatement(sql);
            
            pstm.setInt(1, ItemID);
            pstm.setString(2, l);
            pstm.executeUpdate();
        }
    }
  
    public static ArrayList<String> getItemCategories(Connection conn, Integer ItemID) throws SQLException{
        
        ArrayList<String> ItemCategoryList=new ArrayList<String>();
        
        String sql = "SELECT * FROM Category WHERE Category.ItemID = ?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, ItemID);
        ResultSet rs = pstm.executeQuery();
        
        System.out.println("Categories for id " + ItemID + "<");
        while (rs.next()){
            ItemCategoryList.add(rs.getString("Kind"));
            System.out.println(rs.getString("Kind"));
        }
        
        System.out.println("Categories />");
        return ItemCategoryList;
    }
    
    public static float findRating(Connection conn, String Table, int SellerID) throws SQLException{

        String sql = "SELECT * FROM " + Table + " WHERE " + Table +".ID= ?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, SellerID);
        ResultSet rs = pstm.executeQuery();
        
        if (rs.next()){
            String Result = rs.getString("Rating");
            if ("NULL".equals(Result)){
                return -1;
            }
            else return Float.parseFloat(Result);
        }
        return -2;
    }
    
    public static void copyUnconfirmedUserToUsers(Connection conn, String Username) throws SQLException{
        String sql = "INSERT INTO User (SELECT * FROM UnconfirmedUser WHERE Username=?)";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, Username);
        pstm.executeUpdate();
    }
    
    public static void removeUnconfirmedUser(Connection conn, String Username) throws SQLException{
        String sql = "DELETE FROM UnconfirmedUser WHERE Username=?";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, Username);
        pstm.executeUpdate();
    }
    
    public static ArrayList<Item> findSellingItems(Connection conn, int UserID) throws SQLException{
        
        String sql = "SELECT * FROM Item WHERE SellerID =?";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, UserID);
        ResultSet rs = pstm.executeQuery();
        
        ArrayList<Item> Items = new ArrayList<Item>();
        while (rs.next()) {
            int ID = rs.getInt("ID");
            String Name = rs.getString("Name");
            float CurrPrice = rs.getFloat("CurrPrice");
            float BuyPrice = rs.getFloat("BuyPrice");
            String StrFirstBid = rs.getString("FirstBid");
            float FirstBid=-1;
            if (StrFirstBid==null){
                FirstBid = CurrPrice;
            }
            else{
                FirstBid = Integer.parseInt(StrFirstBid);
            }
            String StrNumberOfBids = rs.getString("NumberOfBids"); 
            int NumberOfBids = Integer.parseInt(StrNumberOfBids);
            String StrBidsID = rs.getString("BidsID");
            int BidsID=-1;
            if (StrBidsID==null){
                BidsID = 0;
            }
            else{
                BidsID = Integer.parseInt(StrBidsID);
            }
            int LocationID = rs.getInt("LocationID");
            java.util.Date Start = rs.getDate("Start");
            java.util.Date End = rs.getDate("End");
            int SellerID = rs.getInt("SellerID");
            String ItemDescription = rs.getString("ItemDescription");
            Item item = new Item(ID, Name, CurrPrice, BuyPrice, FirstBid, NumberOfBids, BidsID, LocationID, Start, End, SellerID, ItemDescription);
            
            Items.add(item);
        }
        
        return Items;
    }
    
    public static ArrayList<Item> findBiddingItems(Connection conn, int UserID) throws SQLException{

        System.out.println(UserID);
        String sql = "SELECT DISTINCT(Item.ID) FROM Item,Bid,User WHERE (Item.BidsID = Bid.BidsID AND Bid.bidderID = ?)";
        String sql2 = "SELECT * FROM Item WHERE ID=?";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, UserID);
        ResultSet rs = pstm.executeQuery();

        ArrayList<Item> Items = new ArrayList<Item>();
        while (rs.next()) {
            
            PreparedStatement pstm2 = conn.prepareStatement(sql2);
            int ItemID = rs.getInt("ID");
            pstm2.setInt(1,ItemID);
            ResultSet rs2 = pstm2.executeQuery();
            
            if (rs2.next()){
                
                int ID = rs2.getInt("ID");
                String Name = rs2.getString("Name");;
                float CurrPrice = rs2.getFloat("CurrPrice");
                float BuyPrice = rs2.getFloat("BuyPrice");
                String StrFirstBid = rs2.getString("FirstBid");
                float FirstBid=-1;
                if (StrFirstBid == null){
                    FirstBid = CurrPrice;
                }
                else{
                    FirstBid = Integer.parseInt(StrFirstBid);
                }
                String StrNumberOfBids = rs2.getString("NumberOfBids"); 
                int NumberOfBids = Integer.parseInt(StrNumberOfBids);
                String StrBidsID = rs2.getString("BidsID");
                int BidsID=-1;
                if (StrBidsID == null){
                    BidsID = 0;
                }
                else{
                    BidsID = Integer.parseInt(StrBidsID);
                }
                int LocationID = rs2.getInt("LocationID");
                java.util.Date Start = rs2.getDate("Start");
                java.util.Date End = rs2.getDate("End");
                int SellerID = rs2.getInt("SellerID");
                String ItemDescription = rs2.getString("ItemDescription");
                
                Item item = new Item(ID, Name, CurrPrice, BuyPrice, FirstBid, NumberOfBids, BidsID, LocationID, Start, End, SellerID, ItemDescription);
            
                Items.add(item);
            }
        }
        return Items;

    }

    public static void updateItemBiddingInfo(Connection conn, float CurrPrice, float FirstBid, int NumberOfBids, int BidsID, int ItemID) throws SQLException{

        String sql = "UPDATE Item SET CurrPrice=?, FirstBid=?, NumberOfBids=?, BidsID=?  where ID=?";
 
      PreparedStatement pstm = conn.prepareStatement(sql);
 
      pstm.setFloat(1, CurrPrice);
      pstm.setFloat(2, FirstBid);
      pstm.setInt(3, NumberOfBids);
      pstm.setInt(4, BidsID);
      pstm.setInt(5, ItemID);
      pstm.executeUpdate();

    }

    public static void placeBid(Connection conn, int BidsID, int BidderID, String BidTime, float BidAmount) throws SQLException{
        
        String sql = "INSERT INTO Bid (bidID, bidsID, bidderID, bidTime, bidAmount) values (?,?,?,?,?)";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setInt(1, 0);
        pstm.setInt(2, BidsID);
        pstm.setInt(3, BidderID);
        pstm.setString(4, BidTime);
        pstm.setFloat(5, BidAmount);
        
        pstm.executeUpdate();
        
    }

    public static void updateItemFromID(Connection conn, Item item, Integer id) throws SQLException {
        
        String sql = "UPDATE Item SET FirstBid=?, NumberOfBids=?, BidsID=?  where ID=?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setInt(1, item.getId());
        pstm.setString(2, item.getName());
        pstm.setFloat(3, item.getCurrPrice());
        pstm.setFloat(4, item.getBuyPrice());
        pstm.setFloat(5, item.getFirstBid());
        pstm.setInt(6, item.getNumberOfBids());
        pstm.setInt(7, item.getBidsID());
        pstm.setInt(8, item.getLocationID());
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd hh:mm:ss");
        pstm.setString(9, ft.format(item.getStart()));
        pstm.setString(10, ft.format(item.getEnd()));
        pstm.setInt(11, item.getSellerID());
        pstm.setString(12, item.getItemDescription());
        
        
        pstm.executeUpdate();
    }

    public static void updateLocationFromID(Connection conn, Location location, int locationID) throws SQLException {
         
        String sql = "UPDATE Item SET ID=?, Address=?, Country=?, PostCode=?  where ID=?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setInt(1, location.getId());
        pstm.setString(2, location.getAddress());
        pstm.setString(3, location.getCountry());
        pstm.setString(4, location.getPostCode());
        pstm.setInt(5, locationID);
        
        pstm.executeUpdate();
    }

    public static void updateItemCategoriesFromID(Connection conn, ArrayList<String> Categories, int ItemID) throws SQLException {
        
        String sqlDelete = "DELETE FROM Category WHERE ItemID=? AND Kind=?";
        String sqlInsert = "INSERT INTO Category (ItemID,Kind) values (?,?)";
        
        for (String c:Categories){
            
            PreparedStatement pstmDelete = conn.prepareStatement(sqlDelete);
            pstmDelete.setInt(1, ItemID);
            pstmDelete.setString(2, c);
            pstmDelete.executeUpdate();

            PreparedStatement pstmInsert = conn.prepareStatement(sqlInsert);
            pstmInsert.setInt(1, ItemID);
            pstmInsert.setString(2, c);
            pstmInsert.executeUpdate();
        }
        
    }

    public static Bid findLastBid(Connection conn, int BidderID, int ItemID) throws SQLException{

        String sql = "SELECT b.* FROM Bid b, Item i WHERE b.bidderID=? AND i.BidsID=b.bidsID AND i.ID=? ORDER BY b.bidTime DESC";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, BidderID);
        pstm.setInt(2, ItemID);
        ResultSet rs = pstm.executeQuery();
            
        if (rs.next()){
            int bidID = rs.getInt("bidID");
            int bidsID = rs.getInt("bidsID");
            int bidderID = rs.getInt("bidderID");
            Date bidTime = rs.getDate("bidTime");
            float bidAmount = rs.getFloat("bidAmount");

            Bid bid=new Bid(bidID, bidsID, bidderID, bidTime, bidAmount);
            return bid;
        }    
        
        return null;
    }

    public static void insertMessage(Connection conn, int ReceiverID, int SenderID, String Message) throws SQLException{

        String sql = "INSERT INTO Message (ID,Receiver,Sender,IM,Status) values (?,?,?,?,?)";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, 0);
        pstm.setInt(2, ReceiverID);
        pstm.setInt(3, SenderID);
        pstm.setString(4, Message);
        pstm.setInt(5, 1);
        
        pstm.executeUpdate();
        
    }

    public static ArrayList<Message> findIncomingMessages(Connection conn, int RecID) throws SQLException {
        String sql = "Select * FROM Message WHERE Receiver = ? ORDER BY Sender DESC, Status DESC";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, RecID);
        ResultSet rs = pstm.executeQuery();
            
        ArrayList<Message> Messages = new ArrayList<Message>();
        
        while (rs.next()){
            int ID = rs.getInt("ID");
            int ReceiverID = rs.getInt("Receiver");
            int SenderID = rs.getInt("Sender");
            String IM = rs.getString("IM");
            int Status = rs.getInt("Status");

            Message message=new Message(ID, ReceiverID, SenderID, IM, Status);
            Messages.add(message);
            
        }    
        return Messages;
        
    }

    public static void updateIncomingMessage(Connection conn, int MessageID) throws SQLException{

        String sql = "UPDATE Message SET Status=? where ID=?";

        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, 0);
        pstm.setInt(2, MessageID);
        
        pstm.executeUpdate();

    }

    public static ArrayList<Message> findOutgoingMessages(Connection conn, int SendID) throws SQLException{
        String sql = "Select * FROM Message WHERE Sender = ? ORDER BY Receiver DESC";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, SendID);
        ResultSet rs = pstm.executeQuery();
            
        ArrayList<Message> Messages = new ArrayList<Message>();
        
        while (rs.next()){
            int ID = rs.getInt("ID");
            int ReceiverID = rs.getInt("Receiver");
            int SenderID = rs.getInt("Sender");
            String IM = rs.getString("IM");
            int Status = rs.getInt("Status");

            Message message=new Message(ID, ReceiverID, SenderID, IM, Status);
            Messages.add(message);
            
        }    
        return Messages;
    }

    public static void updateSellerRating(Connection conn, int SellerID, float SellerRating) throws SQLException {
        
        
        String sql2 = "Select Rating, Count FROM Seller WHERE ID=?";
        PreparedStatement pstm2 = conn.prepareStatement(sql2);
        pstm2.setFloat(1, SellerID);
        ResultSet rs = pstm2.executeQuery();
        
        if (rs.next()){
            String sql = "UPDATE Seller SET Rating=?, Count=? where ID=?";
            PreparedStatement pstm = conn.prepareStatement(sql);
            
            float oldRating = rs.getFloat("Rating");
            int Count = rs.getInt("Count");
            int newRating;
           
            pstm.setFloat(1, oldRating + (SellerRating-oldRating)/(Count+1));
            pstm.setFloat(2, Count+1);
            pstm.setInt(3, SellerID);

            pstm.executeUpdate();

            return;
        }
        else{
            String sql = "INSERT INTO Seller (ID, Rating, Count) values (?,?,?)";
            PreparedStatement pstm = conn.prepareStatement(sql);
            
            pstm.setInt(1, SellerID);
            pstm.setFloat(2, SellerRating);
            pstm.setInt(3, 1);

            pstm.executeUpdate();
        }
        
        return;
    }
    
    public static void updateBidderRating(Connection conn, int BidderID, float BidderRating) throws SQLException {
        
        
        String sql2 = "Select Rating, Count FROM Seller WHERE ID=?";
        PreparedStatement pstm2 = conn.prepareStatement(sql2);
        pstm2.setFloat(1, BidderID);
        ResultSet rs = pstm2.executeQuery();
        
        if (rs.next()){
            String sql = "UPDATE Bidder SET Rating=?, Count=? where ID=?";
            PreparedStatement pstm = conn.prepareStatement(sql);
            
            float oldRating = rs.getFloat("Rating");
            int Count = rs.getInt("Count");
            int newRating;
           
            pstm.setFloat(1, oldRating + (BidderRating-oldRating)/(Count+1));
            pstm.setFloat(2, Count+1);
            pstm.setInt(3, BidderID);

            pstm.executeUpdate();

            return;
        }
        else{
            String sql = "INSERT INTO Bidder (ID, Rating, Count) values (?,?,?)";
            PreparedStatement pstm = conn.prepareStatement(sql);
            
            pstm.setInt(1, BidderID);
            pstm.setFloat(2, BidderRating);
            pstm.setInt(3, 1);

            pstm.executeUpdate();
        }
        
        return;
    }

    public static ArrayList<Item> getRecomendedItems(Connection conn, int BidderID) throws SQLException {
        
        class BiddedCategory{

          private BiddedCategory(String Cat1, String Cat2, String Cat3, String Cat4, String Cat5) {
              throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          }
        
        };
        
        // find which categories the bidder has bidded
        String sql = "SELECT b.* FROM BiddedCategory b WHERE b.ID = BidderID";
        
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, BidderID);
        ResultSet rs = pstm.executeQuery();
            
        if (rs.next()){
            String Cat1 = rs.getString("Cat1");
            String Cat2 = rs.getString("Cat2");
            String Cat3 = rs.getString("Cat3");
            String Cat4 = rs.getString("Cat4");
            String Cat5 = rs.getString("Cat5");

            BiddedCategory myBiddedCategory=new BiddedCategory(Cat1, Cat2, Cat3, Cat4, Cat5);
        } 
        
        // find which categories all the bidder have bidded
        sql = "SELECT b.* FROM BiddedCategory b WHERE b.ID != BidderID";
        
        pstm = conn.prepareStatement(sql);
        pstm.setInt(1, BidderID);
        rs = pstm.executeQuery();
            
        ArrayList<BiddedCategory> BiddedCategories = new ArrayList<BiddedCategory>();
        
        while (rs.next()){
            String Cat1 = rs.getString("Cat1");
            String Cat2 = rs.getString("Cat2");
            String Cat3 = rs.getString("Cat3");
            String Cat4 = rs.getString("Cat4");
            String Cat5 = rs.getString("Cat5");

            BiddedCategory biddedCategory=new BiddedCategory(Cat1, Cat2, Cat3, Cat4, Cat5);
       
            BiddedCategories.add(biddedCategory);
        
        } 
        
        ArrayList<Integer> collaborativeNeighboursIDs = null;
        
        // collaborativeNeighboursIDs = findMatchInCategories(myBiddedCategory, myBiddedCategories);
        
        sql = "SELECT i.* FROM Bidder B, Bid b, Item i, Seller s "
            + "WHERE "
                + "B.ID=b.bidderID AND "
                + "b.bidsID = Item.bidsID AND "
                + "s.ID = i.SellerID"
            + "ORDER BY s.Rating Limit 10";
        
        pstm = conn.prepareStatement(sql);
        pstm.setInt(1, BidderID);
        rs = pstm.executeQuery();
            
        ArrayList<Item> Items = new ArrayList<Item>();
        
        while (rs.next()){
            int ID = rs.getInt("ID");
            String Name = rs.getString("Name");
            float CurrPrice = rs.getFloat("CurrPrice");
            float BuyPrice = rs.getFloat("BuyPrice");
            String StrFirstBid = rs.getString("FirstBid");
            float FirstBid=-1;
            if (StrFirstBid==null){
                FirstBid = CurrPrice;
            }
            else{
                FirstBid = Integer.parseInt(StrFirstBid);
            }
            String StrNumberOfBids = rs.getString("NumberOfBids"); 
            int NumberOfBids = Integer.parseInt(StrNumberOfBids);
            String StrBidsID = rs.getString("BidsID");
            int BidsID=-1;
            if (StrBidsID==null){
                BidsID = 0;
            }
            else{
                BidsID = Integer.parseInt(StrBidsID);
            }
            int LocationID = rs.getInt("LocationID");
            java.util.Date Start = rs.getDate("Start");
            java.util.Date End = rs.getDate("End");
            int SellerID = rs.getInt("SellerID");
            String ItemDescription = rs.getString("ItemDescription");
            Item item = new Item(ID, Name, CurrPrice, BuyPrice, FirstBid, NumberOfBids, BidsID, LocationID, Start, End, SellerID, ItemDescription);
            
            Items.add(item);
        } 

        return Items;
        
    }

}