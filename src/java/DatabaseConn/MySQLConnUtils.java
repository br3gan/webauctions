/*

 */

/**
 * @author racoon
 * @brief MySQL Database Utilities.
 * This is the low-level database utility implementation. An upper layer
 * will be added to access these below functions.
 * This way we can access an other Database (SQLServer or Oracle) without
 * changing the concrete layer of deployment.
 */
package DatabaseConn;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
 
public class MySQLConnUtils {
 
public static Connection getMySQLConnection()
        throws ClassNotFoundException, SQLException {
  
    // Database info.
    // Need to be changed accordingly
    String hostName = "localhost";
    String dbName = "mydb";
    String userName = "root";
    String password = "root";
    return getMySQLConnection(hostName, dbName, userName, password);
}
 
public static Connection getMySQLConnection(String hostName, String dbName,
        String userName, String password) throws SQLException,
        ClassNotFoundException {
    
    // class Driver for MySQL Database
    // maybe not needed for java 6 and above
    Class.forName("com.mysql.jdbc.Driver");
 
 
    // Compose connection URL
    String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName;
 
    Connection conn = DriverManager.getConnection(connectionURL, userName,
            password);
    return conn;
}
}