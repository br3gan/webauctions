package Servlets;
 
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import Entities.User;
import DatabaseUtils.*;
import Entities.Admin;
import Entities.Item;
import Entities.Location;
import Entities.UnconfirmedUser;
import java.util.ArrayList;
 
@WebServlet(urlPatterns = { "/doLogin" })
public class DoLoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public DoLoginServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        String Username = request.getParameter("Username");
        String Password = request.getParameter("Password");
        String rememberMeStr = request.getParameter("rememberMe");
        boolean remember= "Y".equals(rememberMeStr);
 
        System.out.println("Username:"+Username);
        System.out.println("Password:"+Password);/*
        System.out.println("Firstname:"+ Firstname);
        System.out.println("Lastname:"+ Lastname);
        System.out.println("Mail:"+ Mail);
        System.out.println("Telephone:"+ Telephone);
        System.out.println("Country:"+ Country);
        System.out.println("Address:"+ Address);
        System.out.println("SSN:"+ SSN);*/ 
        
        User user = null;
        Admin admin = null;
        boolean hasError = false;
        String errorString = null;
        
        if (Username == null || Password == null
                 || Username.length() == 0 || Password.length() == 0) {
            hasError = true;
            errorString = "Required username and password!";
        } else {
            Connection conn = DatabaseStoreUtils.getStoredConnection(request);
            try {
                admin = DatabaseAccessUtils.findAdmin(conn, Username, Password);
                if (admin != null) {
                
                    //get All Items
                    ArrayList<User> Users = null;
                    
                    Users = DatabaseAccessUtils.findUsers(conn);
                    if (Users == null){
                        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/adminHomeView.jsp");
                        dispatcher.forward(request, response);
                    }
                    ArrayList<Float> SellingRatings = new ArrayList<>();
                    ArrayList<Float> BiddingRatings = new ArrayList<>();
                     
                    ArrayList<Location> Locations = new ArrayList<Location>();
                    
                    for (User u:Users){
                        // get Item's location
                        Locations.add( DatabaseAccessUtils.findLocationFromID(conn, u.getLocationID()) );
                        // get Item's Seller Selling Rating
                        SellingRatings.add( DatabaseAccessUtils.findRating(conn, "Seller", u.getId()) );
                        // get Item's Seller Bidding Ratings
                        BiddingRatings.add( DatabaseAccessUtils.findRating(conn, "Bidder", u.getId()) );
                    }
                    System.out.println();
                    // Store information in request attribute, before forward.
                    request.setAttribute("admin", admin);
                    request.setAttribute("Users", Users);
                    request.setAttribute("Locations", Locations);
                    request.setAttribute("SellingRatings", SellingRatings);
                    request.setAttribute("BiddingRatings", BiddingRatings);
                    
                    //get All Items
                    ArrayList<UnconfirmedUser> UncUsers = null;
                    
                    UncUsers = DatabaseAccessUtils.findUnconfirmedUsers(conn);
                                         
                    ArrayList<Location> UncLocations = new ArrayList<Location>();
                    
                    for (UnconfirmedUser u:UncUsers){
                        // get Item's location
                        UncLocations.add( DatabaseAccessUtils.findLocationFromID(conn, u.getLocationID()) );
                    }
                    
                    request.setAttribute("UnconfirmedUsers", UncUsers);
                    request.setAttribute("UnconfirmedLocations", UncLocations);
 
                    HttpSession session = request.getSession();
                    User Auser = new User(admin.getId(), admin.getUsername(), admin.getPassword(), admin.getPassword(), admin.getLastname(), admin.getMail(), admin.getTelephone(), 0, admin.getSsn());
                    DatabaseStoreUtils.storeLoginedUser(session, Auser);
                    
                    // Forward to /WEB-INF/views/login.jsp
                    RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/admin/adminHomeView.jsp");
 
                    dispatcher.forward(request, response);
                    return;
                }
                else {
                    user = DatabaseAccessUtils.findUser(conn, Username, Password);
                    if (user == null) {
                        System.out.println();
                        hasError = true;
                        errorString = "User Name or password invalid";
                    }
                }
                
            } catch (SQLException e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();
            }
        }
        // If error, forward to /WEB-INF/views/loginHomeViewView.jsp
        if (hasError) {
            user = new User();
            user.setUsername(Username);
            user.setPassword(Password);
        
            // Store information in request attribute, before forward.
            request.setAttribute("errorString", errorString);
            request.setAttribute("user", user);
 
            // Forward to /WEB-INF/views/login.jsp
            RequestDispatcher dispatcher //
            = this.getServletContext().getRequestDispatcher("/WEB-INF/views/homeView.jsp");
 
            dispatcher.forward(request, response);
        }
     
        // If no error
        // Store user information in Session
        // And redirect to userInfo page.
        else {
            
            HttpSession session = request.getSession();
            DatabaseStoreUtils.storeLoginedUser(session, user);
             
             // If user checked "Remember me".
            if(remember)  {
                DatabaseStoreUtils.storeUserCookie(response,user);
            }
    
            // Else delete cookie.
            else  {
                DatabaseStoreUtils.deleteUserCookie(response);
            }                      
            
            // Redirect to userInfo page.
            response.sendRedirect(request.getContextPath() + "/userHome");
        }
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}