/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.bidder;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Category;
import Entities.Item;
import Entities.Location;
import Entities.User;
import Servlets.admin.AcceptRegistrationServlet;
import java.io.IOException; 
import java.sql.Connection;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = { "/placeBid"})
public class PlaceBidServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public PlaceBidServlet() {
        super();
    }    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int ItemID =  Integer.parseInt((String) request.getParameter("ItemID"));
        float BidAmount =  Float.parseFloat((String) request.getParameter("BidAmount"));
        
        Item item = null;
        
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        // get Item's info
        try {
            // get stored user
            HttpSession session = request.getSession();
            User loginedUser = DatabaseStoreUtils.getLoginedUser(session);

            item = DatabaseAccessUtils.findItem(conn, ItemID);
            item.printItem();
            
            int BidsID = -1;
            float FirstBid = -1;
            float CurrPrice = -1;
            // if item has not been hit by any bidder so far
            if (item.getNumberOfBids() == 0){
                // create a new bidsID
                BidsID = DatabaseAccessUtils.getLastFieldInt(conn, "bidsID", "Bid");
                // if there are not any bits so far, assign first bidsID as 1
                if (BidsID == -1){
                    BidsID = 1;
                }
                // else get last BidsID and increase by one
                else {
                    BidsID += 1;
                }
            }
            // else take the old ones
            else {
                BidsID = item.getBidsID();
            }
            FirstBid = item.getFirstBid();
            CurrPrice = BidAmount;
            
            // update Item info
            DatabaseAccessUtils.updateItemBiddingInfo(conn, CurrPrice, FirstBid, item.getNumberOfBids() + 1, BidsID, item.getId());

            Date javaDate = new java.util.Date();
            javaDate.getTime();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String BidTime = sdf.format(javaDate);

            // update Bid table
            DatabaseAccessUtils.placeBid(conn, BidsID, loginedUser.getId(), BidTime, BidAmount);

        } catch (SQLException ex) {
            Logger.getLogger(AcceptRegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute("Item", item);

        RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/bidder");
 
                    dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
    
}
