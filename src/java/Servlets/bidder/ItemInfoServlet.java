/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.bidder;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Category;
import Entities.Item;
import Entities.Location;
import Servlets.admin.AcceptRegistrationServlet;
import java.io.IOException; 
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author racoon
 */
@WebServlet(urlPatterns = { "/itemInfo"})
public class ItemInfoServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ItemInfoServlet() {
        super();
    }    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int ItemID =  Integer.parseInt((String) request.getParameter("ItemID"));
        Item item = null;
        float SellerRating = -1;
        String SellerUsername = null;
        Location location = null;
        
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        // get Item's and Seller's info
        try {
             item = DatabaseAccessUtils.findItem(conn, ItemID);
             location = DatabaseAccessUtils.findLocationFromID(conn, item.getLocationID());
             SellerRating = DatabaseAccessUtils.findRating(conn, "Seller", item.getSellerID());
             SellerUsername = DatabaseAccessUtils.findSellerUsername(conn, item.getSellerID());
        } catch (SQLException ex) {
            Logger.getLogger(AcceptRegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        request.setAttribute("Item", item);
        request.setAttribute("Location", location);
        request.setAttribute("Rating", SellerRating);
        request.setAttribute("SellerUserame", SellerUsername);
        
        RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/itemInfoView.jsp");
 
                    dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
    
}
