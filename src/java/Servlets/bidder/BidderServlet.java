package Servlets.bidder;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Category;
import Entities.Item;
import Entities.Location;
import Entities.User;
import java.io.IOException; 
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
@WebServlet(urlPatterns = { "/bidder"})
public class BidderServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public BidderServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // query all bids
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        ArrayList<Item> Items = null;
        
        try {
            Items = DatabaseAccessUtils.findItems(conn);
            if (Items == null){
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/bidderView.jsp");
                dispatcher.forward(request, response);
            }
            ArrayList<Location> Locations = new ArrayList<Location>();
            ArrayList<ArrayList<String>> ListOfItemCategories = new ArrayList<ArrayList<String>>();
            
            int index=0;
            for (Item i:Items){
                System.out.println("Item: " + index);
                i.printItem();
                // get Item's Location
                Locations.add(DatabaseAccessUtils.findLocationFromID(conn, i.getLocationID()));
                Locations.get(index).printLocation();
                // get Items's Categories
                ListOfItemCategories.add(DatabaseAccessUtils.getItemCategories(conn, i.getId()));
                System.out.println("Categories: ");
                for (String s : ListOfItemCategories.get(index))
                    System.out.print(s + " ");
                System.out.println();
                index++;
            }
            request.setAttribute("Items", Items);
            request.setAttribute("Locations", Locations);
            request.setAttribute("ListOfItemCategories", ListOfItemCategories);
            System.out.println("Did that");
            System.out.println(Items.size());
            System.out.println(Locations.size());
            System.out.println(ListOfItemCategories.size());
        } catch (SQLException ex) {
            Logger.getLogger(BidderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        HttpSession session = request.getSession();
        User logineduser = DatabaseStoreUtils.getLoginedUser(session);
        RequestDispatcher dispatcher;
        if (logineduser != null){
            dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/bidderView.jsp");
        }
        else {
            dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/tourView.jsp");
        }
        dispatcher.forward(request, response);
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}
