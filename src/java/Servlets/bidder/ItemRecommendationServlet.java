package Servlets.bidder;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Category;
import Entities.Item;
import Entities.Location;
import Entities.User;
import java.io.IOException; 
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
@WebServlet(urlPatterns = { "/recommendItem"})
public class ItemRecommendationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ItemRecommendationServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        User logineduser = DatabaseStoreUtils.getLoginedUser(session);
        
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        
        ArrayList<Item> Items = null;
        ArrayList<Location> Locations = new ArrayList<Location>();
        ArrayList<ArrayList<String>> Categories = new ArrayList<ArrayList<String>>();
        try {
            Items = DatabaseAccessUtils.getRecomendedItems(conn, logineduser.getId());
            if (Items == null){
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/bidderView.jsp");
                dispatcher.forward(request, response);
                return;
            }
            
            for (Item i:Items){
                Locations.add(DatabaseAccessUtils.findLocationFromID(conn, i.getLocationID()));
                Categories.add(DatabaseAccessUtils.getItemCategories(conn, i.getId()));
            }
                
            request.setAttribute("Items", Items);
            request.setAttribute("Locations", Locations);
            request.setAttribute("ListOfItemCategories", Categories);
        } catch (SQLException ex) {
            Logger.getLogger(BidderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        RequestDispatcher dispatcher;
        dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/recommendedItemsView.jsp");

        dispatcher.forward(request, response);
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}
