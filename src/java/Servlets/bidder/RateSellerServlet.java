/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.bidder;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Category;
import Entities.Item;
import Entities.Location;
import Entities.User;
import Servlets.admin.AcceptRegistrationServlet;
import java.io.IOException; 
import java.sql.Connection;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author racoon
 */
@WebServlet(urlPatterns = { "/rateSeller"})
public class RateSellerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public RateSellerServlet() {
        super();
    }    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int SellerID =  Integer.parseInt((String) request.getParameter("SellerID"));
        float SellerRating =  Float.parseFloat((String) request.getParameter("SellerRating"));
        
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);

        try {
            // update SellerRating
            DatabaseAccessUtils.updateSellerRating(conn, SellerID, SellerRating);
        } catch (SQLException ex) {
            Logger.getLogger(RateSellerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
       

        RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/home");
 
                    dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
    
}
