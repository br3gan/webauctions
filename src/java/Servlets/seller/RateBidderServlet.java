/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.bidder;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Category;
import Entities.Item;
import Entities.Location;
import Entities.User;
import Servlets.admin.AcceptRegistrationServlet;
import java.io.IOException; 
import java.sql.Connection;
import java.util.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author racoon
 */
@WebServlet(urlPatterns = { "/rateBidder"})
public class RateBidderServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public RateBidderServlet() {
        super();
    }    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int BidderID =  Integer.parseInt((String) request.getParameter("BidderID"));
        float BidderRating =  Float.parseFloat((String) request.getParameter("BidderRating"));
        
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);

        try {
            // update BidderRating
            DatabaseAccessUtils.updateBidderRating(conn, BidderID, BidderRating);
        } catch (SQLException ex) {
            Logger.getLogger(RateBidderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
       

        RequestDispatcher dispatcher //
                    = this.getServletContext().getRequestDispatcher("/home");
 
                    dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
    
}
