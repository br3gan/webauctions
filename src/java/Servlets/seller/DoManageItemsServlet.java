/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.seller;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Category;
import Entities.Item;
import Entities.Location;
import Entities.User;
import java.io.IOException; 
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
@WebServlet(urlPatterns = { "/doManageItems"})
public class DoManageItemsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public DoManageItemsServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // query all bids
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        
        HttpSession session = request.getSession();
        Item item = (Item) session.getAttribute("Item");
        Location location = (Location) session.getAttribute("Location");
        ArrayList<String> Categories = (ArrayList<String>) session.getAttribute("Categories");
        
        int index=0;
        System.out.println("Item: " + index);
        item.printItem();
        try {
            // update Item's values
            DatabaseAccessUtils.updateItemFromID(conn, item, item.getId());
        } catch (SQLException ex) {
            Logger.getLogger(DoManageItemsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            // get Item's Location
            DatabaseAccessUtils.updateLocationFromID(conn, location, item.getLocationID());
        } catch (SQLException ex) {
            Logger.getLogger(DoManageItemsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            // get Items's Categories
            DatabaseAccessUtils.updateItemCategoriesFromID(conn, Categories, item.getId());
        } catch (SQLException ex) {
            Logger.getLogger(DoManageItemsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println();
        
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/seller/sellerItemsView.jsp");
        dispatcher.forward(request, response);
   }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}