/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.seller;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Category;
import Entities.Item;
import Entities.Location;
import Entities.User;
import java.io.IOException; 
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
@WebServlet(urlPatterns = { "/manageItems"})
public class ManageItemsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ManageItemsServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // query all bids
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        ArrayList<Item> Items = null;
        
        HttpSession session = request.getSession();
        User loginedUser = DatabaseStoreUtils.getLoginedUser(session);
        
        try {
            Items = DatabaseAccessUtils.findItems(conn, loginedUser.getId());
            if (Items == null){
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/seller/sellerView.jsp");
                dispatcher.forward(request, response);
            }
            ArrayList<Location> Locations = new ArrayList<Location>();
            ArrayList<ArrayList<String>> ListOfItemCategories = new ArrayList<ArrayList<String>>();
            
            int index=0;
            for (Item i:Items){
                System.out.println("Item: " + index);
                i.printItem();
                // get Item's Location
                Locations.add(DatabaseAccessUtils.findLocationFromID(conn, i.getLocationID()));
                Locations.get(index).printLocation();
                // get Items's Categories
                ListOfItemCategories.add(DatabaseAccessUtils.getItemCategories(conn, i.getId()));
                System.out.println("Categories: ");
                for (String s : ListOfItemCategories.get(index))
                    System.out.print(s + " ");
                System.out.println();
                index++;
            }
            request.setAttribute("Items", Items);
            request.setAttribute("Locations", Locations);
            request.setAttribute("ListOfItemCategories", ListOfItemCategories);
            
        } catch (SQLException ex) {
            Logger.getLogger(SellerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/seller/sellerItemsView.jsp");
        dispatcher.forward(request, response);
   }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}