///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
package Servlets.seller;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet(urlPatterns = { "/doCreateBid" })
public class DoCreateBidServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public DoCreateBidServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        String Name = request.getParameter("Name");
        float CurrPrice = Float.parseFloat(request.getParameter("CurrPrice"));
        float BuyPrice = Float.parseFloat(request.getParameter("BuyPrice"));
        Date Startdate = null;
        String Start = request.getParameter("Start");
        try {
            Startdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(Start.replace('T', ' '));
        } catch (ParseException ex) {
            Logger.getLogger(DoCreateBidServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date Enddate = null;
        String End = request.getParameter("End");
        try {
            Enddate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(End.replace('T', ' '));
        } catch (ParseException ex) {
            Logger.getLogger(DoCreateBidServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        String ItemDescription = request.getParameter("ItemDescription");
        String Address = request.getParameter("Address");
        String Country = request.getParameter("Country");
        String Postcode = request.getParameter("Postcode");
        ArrayList<String> Categories = new ArrayList<String>(Arrays.asList(request.getParameterValues("Categories[]")));
        
        User user = new User();
        HttpSession session = request.getSession();
        user = DatabaseStoreUtils.getLoginedUser(session);
        
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        
        // check if location exists
        Location location = null;
        try {
            int LocationID = DatabaseAccessUtils.findLocation(conn, Country, Address, Postcode);
            if( LocationID==-1 ){
                location = new Location(0, Address, Country, Postcode);
                DatabaseAccessUtils.insertLocation(conn, location);
                LocationID = DatabaseAccessUtils.getLastFieldInt(conn, "ID", "Location");
            }
            // store item
            Item item = new Item();
            item.setId(0);
            item.setName(Name);
            item.setCurrPrice(CurrPrice);
            item.setBuyPrice(BuyPrice);
            item.setFirstBid(0);
            item.setNumberOfBids(0);
            item.setBidsID(0);
            item.setLocationID(LocationID);
            item.setStart(Startdate);
            item.setEnd(Enddate);
            item.setSellerID(DatabaseAccessUtils.findUser(conn, user.getUsername()).getId());
            item.setItemDescription(ItemDescription);
            
            DatabaseAccessUtils.insertItem(conn, item);
            item.setId(DatabaseAccessUtils.findItemID(conn, Name));
            if (item.getId() == -1){
                // problem
            }   
            DatabaseAccessUtils.insertCaregories(conn, item.getId(), Categories);
            
                        
        } catch (SQLException e) {
            e.printStackTrace();
            String errorString = e.getMessage();
        }
        
        //Redirect to userHome page.
        response.sendRedirect(request.getContextPath() + "/manageItems");
   
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}