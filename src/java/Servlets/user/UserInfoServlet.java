package Servlets.user;
 
import java.io.IOException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import Entities.User;
import DatabaseUtils.*;
import Entities.Bid;
import Entities.Item;
import Entities.Location;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
 
@WebServlet(urlPatterns = { "/userInfo" })
public class UserInfoServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public UserInfoServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
 
        // Check User has logged on
        User loginedUser = DatabaseStoreUtils.getLoginedUser(session);
        
        // Not logged in
        if (loginedUser == null) {
            System.out.println("Null user");
            // Redirect to home page.
            response.sendRedirect(request.getContextPath() + "/");
            return;
        }
 
        request.setAttribute("User", loginedUser);
        
        // Store info in request attribute
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        Location location = null;
        try {
            location = DatabaseAccessUtils.findLocationFromID(conn, loginedUser.getLocationID());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("Location", location);
        
        // get user's Selling and Bidding Items
        int UserID =  loginedUser.getId();
        boolean hasError = false;
        String errorString = null;
        try {
            
            //get All Items
            ArrayList<Item> SellingItems = null;
            ArrayList<Item> BiddingItems = null;

            User user = DatabaseAccessUtils.findUser(conn, UserID);
            if (user == null){
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/adminHomeView.jsp");
                dispatcher.forward(request, response);
                return;
            }
            float SellingRating = DatabaseAccessUtils.findRating(conn, "Seller", user.getId());
            float BiddingRating = DatabaseAccessUtils.findRating(conn, "Bidder", user.getId());

            // get Selling Items
            SellingItems = DatabaseAccessUtils.findSellingItems(conn, user.getId());
            
            // get Selling Items' Locations            
            // get Selling Items' Categories           
            ArrayList<Location> SellingItemsLocations = new ArrayList<Location>();
            ArrayList<ArrayList<String>> SellingItemsCategories = new ArrayList<ArrayList<String>>();
            for (Item i:SellingItems){
                SellingItemsLocations.add(DatabaseAccessUtils.findLocationFromID(conn, i.getLocationID()));
                SellingItemsCategories.add(DatabaseAccessUtils.getItemCategories(conn, i.getLocationID()));
            }

            // get Selling Items
            BiddingItems = DatabaseAccessUtils.findBiddingItems(conn, user.getId());
            
            // get Bidding Items' Locations
            // get Bidding Items' Categories
            ArrayList<Location> BiddingItemsLocations = new ArrayList<Location>();
            ArrayList<ArrayList<String>> BiddingItemsCategories = new ArrayList<ArrayList<String>>();
            ArrayList<Bid> BiddingItemsBidInfo = new ArrayList<Bid>();
            for (Item i:BiddingItems){
                BiddingItemsLocations.add(DatabaseAccessUtils.findLocationFromID(conn, i.getLocationID()));
                BiddingItemsCategories.add(DatabaseAccessUtils.getItemCategories(conn, i.getLocationID()));
                BiddingItemsBidInfo.add(DatabaseAccessUtils.findLastBid(conn, UserID, i.getId()));
            }
            System.out.println();
            // Store information in request attribute, before forward.
            request.setAttribute("User", user);
            request.setAttribute("Location", location);
            request.setAttribute("SellingRating", SellingRating);
            request.setAttribute("BiddingRating", BiddingRating);
            request.setAttribute("SellingItems", SellingItems);
            request.setAttribute("BiddingItems", BiddingItems);
            request.setAttribute("SellingItemsCategories", SellingItemsCategories);
            request.setAttribute("BiddingItemsCategories", BiddingItemsCategories);
            request.setAttribute("SellingItemsLocations", SellingItemsLocations);
            request.setAttribute("BiddingItemsLocations", BiddingItemsLocations);
            request.setAttribute("BiddingItemsBidInfo", BiddingItemsBidInfo);

        } catch (SQLException e) {
            e.printStackTrace();
            hasError = true;
            errorString = e.getMessage();
        }
        
        // Logined, forward to /WEB-INF/views/userInfoView.jsp
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/userInfoView.jsp");
        
        dispatcher.forward(request, response);
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}
