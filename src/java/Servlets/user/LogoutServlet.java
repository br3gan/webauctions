package Servlets;
 
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import Entities.User;
import DatabaseUtils.*;
import Entities.Admin;
import Entities.Item;
import Entities.Location;
import Entities.UnconfirmedUser;
import java.util.ArrayList;
 
@WebServlet(urlPatterns = { "/logout" })
public class LogoutServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public LogoutServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DatabaseStoreUtils.deleteUserCookie(response);
        User loginedUser = null;
        
        HttpSession session = request.getSession();
        session.setAttribute("loginedUser", loginedUser);
        // Redirect to userInfo page.
        response.sendRedirect(request.getContextPath() + "/home");
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}