/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.user;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author racoon
 */
@WebServlet(urlPatterns = { "/doSignup" })
public class DoSignupServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public DoSignupServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String Username = request.getParameter("Username");
        String Password = request.getParameter("Password");
        String Firstname = request.getParameter("Firstname");
        String Lastname = request.getParameter("Lastname");
        String Mail = request.getParameter("Mail");
        String Telephone = request.getParameter("Telephone");
        String Country = request.getParameter("Address");
        String Address = request.getParameter("Country");
        String PostCode = request.getParameter("PostCode");
        String SSN = request.getParameter("SSN");
        
        System.out.println("PostCode "+ PostCode);
        
        UnconfirmedUser user = null;
        Location location = null;
        boolean hasError = false;
        String errorString = null;
        
        if (Username == null || Password == null
                 || Username.length() == 0 || Password.length() == 0) {
            hasError = true;
            errorString = "Required username and Password!";
        } else {
            Connection conn = DatabaseStoreUtils.getStoredConnection(request);
            try {
                user = DatabaseAccessUtils.findUnconfirmedUser(conn, Username);
                User _user = new User(); 
                _user = DatabaseAccessUtils.findUser(conn, Username);
                // if username not in base 
                if (user == null && _user == null) {
                    try {
                        int locationID = DatabaseAccessUtils.findLocation(conn, Country, Address, PostCode);
                        if( locationID==-1 ){
                            location = new Location(0, Address, Country, PostCode);
                            DatabaseAccessUtils.insertLocation(conn, location);
                            locationID = DatabaseAccessUtils.getLastFieldInt(conn, "ID", "Location");
                        }
                        user = new UnconfirmedUser(0, Username, Password, Firstname, Lastname, Mail, Telephone, locationID, SSN);
                        DatabaseAccessUtils.insertUnconfirmedUser(conn, user);
                    } catch (SQLException e) {
                        e.printStackTrace();
                        errorString = e.getMessage();
                    }
                }
                // if user already in any database
                else {    
                    hasError = true;
                    errorString = "This username already exists";
                }
            } catch (SQLException e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();
            }
        }
        // If error, forward to /WEB-INF/views/signupView.jsp
        User _user = new User();
        if (hasError) {
            _user.setUsername(user.getUsername());
        
            // Store information in request attribute, before forward.
            request.setAttribute("errorString", errorString);
            request.setAttribute("user", _user);
 
            // Forward to /WEB-INF/views/signupView.jsp
            RequestDispatcher dispatcher //
            = this.getServletContext().getRequestDispatcher("/WEB-INF/views/signupView.jsp");
 
            dispatcher.forward(request, response);
        }
     
        // If no error
        // Store user information in Session
        // And redirect to userInfo page.
        else {
            
            // HttpSession session = request.getSession();
            // DatabaseStoreUtils.storeLoginedUser(session, user);
            
            // Redirect to userInfo page.
            response.sendRedirect(request.getContextPath() + "/confirmMessage");
            
        }
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}