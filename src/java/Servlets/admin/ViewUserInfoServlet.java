package Servlets.admin;
 
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Bid;
import Entities.Item;
import Entities.Location;
import Entities.UnconfirmedUser;
import Entities.User;
import java.util.ArrayList;
 
@WebServlet(urlPatterns = { "/viewUserInfo" })
public class ViewUserInfoServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public ViewUserInfoServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int UserID =  Integer.parseInt((String) request.getParameter("userID"));
        boolean hasError = false;
        String errorString = null;
        try {
            
            //get All Items
            ArrayList<Item> SellingItems = null;
            ArrayList<Item> BiddingItems = null;

            Connection conn = DatabaseStoreUtils.getStoredConnection(request);
            User user = DatabaseAccessUtils.findUser(conn, UserID);
            if (user == null){
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/adminHomeView.jsp");
                dispatcher.forward(request, response);
                return;
            }
            float SellingRating = DatabaseAccessUtils.findRating(conn, "Seller", user.getId());
            float BiddingRating = DatabaseAccessUtils.findRating(conn, "Bidder", user.getId());
            Location location = DatabaseAccessUtils.findLocationFromID(conn, user.getLocationID());

            // get Selling Items
            SellingItems = DatabaseAccessUtils.findSellingItems(conn, user.getId());
            
            // get Selling Items' Locations            
            // get Selling Items' Categories           
            ArrayList<Location> SellingItemsLocations = new ArrayList<Location>();
            ArrayList<ArrayList<String>> SellingItemsCategories = new ArrayList<ArrayList<String>>();
            for (Item i:SellingItems){
                SellingItemsLocations.add(DatabaseAccessUtils.findLocationFromID(conn, i.getLocationID()));
                SellingItemsCategories.add(DatabaseAccessUtils.getItemCategories(conn, i.getLocationID()));
            }

            // get Selling Items
            BiddingItems = DatabaseAccessUtils.findBiddingItems(conn, user.getId());
            
            // get Bidding Items' Locations
            // get Bidding Items' Categories
            ArrayList<Location> BiddingItemsLocations = new ArrayList<Location>();
            ArrayList<ArrayList<String>> BiddingItemsCategories = new ArrayList<ArrayList<String>>();
            ArrayList<Bid> BiddingItemsBidInfo = new ArrayList<Bid>();
            for (Item i:BiddingItems){
                BiddingItemsLocations.add(DatabaseAccessUtils.findLocationFromID(conn, i.getLocationID()));
                BiddingItemsCategories.add(DatabaseAccessUtils.getItemCategories(conn, i.getLocationID()));
                BiddingItemsBidInfo.add(DatabaseAccessUtils.findLastBid(conn, UserID, i.getId()));
            }
            System.out.println();
            // Store information in request attribute, before forward.
            request.setAttribute("User", user);
            request.setAttribute("Location", location);
            request.setAttribute("SellingRating", SellingRating);
            request.setAttribute("BiddingRating", BiddingRating);
            request.setAttribute("SellingItems", SellingItems);
            request.setAttribute("BiddingItems", BiddingItems);
            request.setAttribute("SellingItemsCategories", SellingItemsCategories);
            request.setAttribute("BiddingItemsCategories", BiddingItemsCategories);
            request.setAttribute("SellingItemsLocations", SellingItemsLocations);
            request.setAttribute("BiddingItemsLocations", BiddingItemsLocations);
            request.setAttribute("BiddingItemsBidInfo", BiddingItemsBidInfo);

            // Forward to /WEB-INF/views/admin/viewUserInfoView.jsp
            RequestDispatcher dispatcher //
            = this.getServletContext().getRequestDispatcher("/WEB-INF/views/admin/viewUserInfoView.jsp");

            dispatcher.forward(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
            hasError = true;
            errorString = e.getMessage();
        }
        
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}