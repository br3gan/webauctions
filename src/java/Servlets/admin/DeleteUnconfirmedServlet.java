package Servlets.admin;
 
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Location;
import Entities.UnconfirmedUser;
import Entities.User;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
 
@WebServlet(urlPatterns = { "/deleteUnconfirmed" })
public class DeleteUnconfirmedServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public DeleteUnconfirmedServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String Username =  (String) request.getParameter("Username");
        System.out.println( " To delete: " + Username);
        
        
        // moves UnconfirmedUser in Users
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        try {
            DatabaseAccessUtils.removeUnconfirmedUser(conn, Username);
        } catch (SQLException ex) {
            Logger.getLogger(AcceptRegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        boolean hasError = false;
        String errorString = null;
        
        try {
            //get All Items
            ArrayList<User> Users = null;

            Users = DatabaseAccessUtils.findUsers(conn);
            if (Users == null){
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/adminHomeView.jsp");
                dispatcher.forward(request, response);
            }
            ArrayList<Float> SellingRatings = new ArrayList<>();
            ArrayList<Float> BiddingRatings = new ArrayList<>();

            ArrayList<Location> Locations = new ArrayList<Location>();

            for (User u:Users){
                // get Item's location
                Locations.add( DatabaseAccessUtils.findLocationFromID(conn, u.getLocationID()) );
                // get Item's Seller Selling Rating
                SellingRatings.add( DatabaseAccessUtils.findRating(conn, "Seller", u.getId()) );
                // get Item's Seller Bidding Ratings
                BiddingRatings.add( DatabaseAccessUtils.findRating(conn, "Bidder", u.getId()) );
            }
            System.out.println();
            // Store information in request attribute, before forward.
            request.setAttribute("admin", null);
            request.setAttribute("Users", Users);
            request.setAttribute("Locations", Locations);
            request.setAttribute("SellingRatings", SellingRatings);
            request.setAttribute("BiddingRatings", BiddingRatings);

            //get All Items
            ArrayList<UnconfirmedUser> UncUsers = null;

            UncUsers = DatabaseAccessUtils.findUnconfirmedUsers(conn);

            ArrayList<Location> UncLocations = new ArrayList<Location>();

            for (UnconfirmedUser u:UncUsers){
                // get Item's location
                UncLocations.add( DatabaseAccessUtils.findLocationFromID(conn, u.getLocationID()) );
            }

            request.setAttribute("UnconfirmedUsers", UncUsers);
            request.setAttribute("UnconfirmedLocations", UncLocations);

            // Forward to /WEB-INF/views/login.jsp
            RequestDispatcher dispatcher //
            = this.getServletContext().getRequestDispatcher("/WEB-INF/views/admin/adminHomeView.jsp");

            dispatcher.forward(request, response);
            
        } catch (SQLException e) {
            e.printStackTrace();
            hasError = true;
            errorString = e.getMessage();
        }
        
        
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}