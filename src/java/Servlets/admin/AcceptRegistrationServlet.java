package Servlets.admin;
 
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Location;
import Entities.UnconfirmedUser;
import Entities.User;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
 
@WebServlet(urlPatterns = { "/acceptRegistration" })
public class AcceptRegistrationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public AcceptRegistrationServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String Username =  (String) request.getParameter("Username");

        // moves UnconfirmedUser in Users
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        try {
            DatabaseAccessUtils.copyUnconfirmedUserToUsers(conn, Username);
        } catch (SQLException ex) {
            Logger.getLogger(AcceptRegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/deleteUnconfirmed");
                        dispatcher.forward(request, response);
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}