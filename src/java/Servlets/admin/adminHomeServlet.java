package Servlets.admin;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Location;
import Entities.UnconfirmedUser;
import Entities.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/adminHome"})
public class adminHomeServlet extends HttpServlet {

    public adminHomeServlet() {
        super();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        try {
            ArrayList<User> Users = null;
                    
            Users = DatabaseAccessUtils.findUsers(conn);
            if (Users == null){
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/bidder/adminHomeView.jsp");
                dispatcher.forward(request, response);
            }
            ArrayList<Float> SellingRatings = new ArrayList<>();
            ArrayList<Float> BiddingRatings = new ArrayList<>();

            ArrayList<Location> Locations = new ArrayList<Location>();

            for (User u:Users){
                // get Item's location
                Locations.add( DatabaseAccessUtils.findLocationFromID(conn, u.getLocationID()) );
                // get Item's Seller Selling Rating
                SellingRatings.add( DatabaseAccessUtils.findRating(conn, "Seller", u.getId()) );
                // get Item's Seller Bidding Ratings
                BiddingRatings.add( DatabaseAccessUtils.findRating(conn, "Bidder", u.getId()) );
            }
            System.out.println();
            // Store information in request attribute, before forward.
            request.setAttribute("Users", Users);
            request.setAttribute("Locations", Locations);
            request.setAttribute("SellingRatings", SellingRatings);
            request.setAttribute("BiddingRatings", BiddingRatings);

            //get All Items
            ArrayList<UnconfirmedUser> UncUsers = null;

            UncUsers = DatabaseAccessUtils.findUnconfirmedUsers(conn);

            ArrayList<Location> UncLocations = new ArrayList<Location>();

            for (UnconfirmedUser u:UncUsers){
                // get Item's location
                UncLocations.add( DatabaseAccessUtils.findLocationFromID(conn, u.getLocationID()) );
            }

            request.setAttribute("UnconfirmedUsers", UncUsers);
            request.setAttribute("UnconfirmedLocations", UncLocations);


                    
            // Forward to /WEB-INF/views/login.jsp
            RequestDispatcher dispatcher //
            = this.getServletContext().getRequestDispatcher("/WEB-INF/views/admin/adminHomeView.jsp");

            dispatcher.forward(request, response);
            return;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/admin/adminHomeView.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}