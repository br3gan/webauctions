/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author racoon
 */

package Servlets.messages;

import java.io.IOException; 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.Message;
import Entities.User;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
 
@WebServlet(urlPatterns = { "/checkOutgoing"})
public class CheckOutgoingServlet extends HttpServlet {
   private static final long serialVersionUID = 1L;
 
    public CheckOutgoingServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        HttpSession session = request.getSession();
        User user = DatabaseStoreUtils.getLoginedUser(session);

        Connection conn = DatabaseStoreUtils.getStoredConnection(request);
        try {
            ArrayList<Message> Messages = new ArrayList<Message>();
            ArrayList<String> Receivers = new ArrayList<String>();
            Messages = DatabaseAccessUtils.findOutgoingMessages(conn, user.getId());
            if (Messages == null){
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/messages");
                dispatcher.forward(request, response);
            }
            for (Message m:Messages)
                Receivers.add(DatabaseAccessUtils.findUser(conn, m.getReceiver()).getUsername() );
            
            request.setAttribute("Sender", user.getUsername());
            request.setAttribute("Messages", Messages);
            request.setAttribute("Receivers", Receivers);
        } catch (SQLException ex) {
            Logger.getLogger(DoSendMessagesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // (Users can not access directly into JSP pages placed in WEB-INF)
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/messages/checkOutgoing.jsp");

        dispatcher.forward(request, response);

    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}
