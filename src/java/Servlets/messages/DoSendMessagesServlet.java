/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author racoon
 */

package Servlets.messages;

import java.io.IOException; 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DatabaseUtils.DatabaseAccessUtils;
import DatabaseUtils.DatabaseStoreUtils;
import Entities.User;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
 
@WebServlet(urlPatterns = { "/doSendMessages"})
public class DoSendMessagesServlet extends HttpServlet {
   private static final long serialVersionUID = 1L;
 
    public DoSendMessagesServlet() {
        super();
    }
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        String ReceiverUsername = request.getParameter("ReceiverUsername");
        String Message = request.getParameter("Message");

        HttpSession session = request.getSession();
        User Sender = DatabaseStoreUtils.getLoginedUser(session);

        Connection conn = DatabaseStoreUtils.getStoredConnection(request);

        try {
            // add e message in database
            User user = DatabaseAccessUtils.findUser(conn, ReceiverUsername);
            if (user!=null)
                DatabaseAccessUtils.insertMessage(conn, user.getId() , Sender.getId(), Message);
            
        } catch (SQLException ex) {
            Logger.getLogger(DoSendMessagesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // (Users can not access directly into JSP pages placed in WEB-INF)
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/messages/sendMessagesView.jsp");

        dispatcher.forward(request, response);

    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
 
}
