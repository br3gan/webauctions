/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author racoon
 */
@Entity
@Table(name = "Category")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c"),
    @NamedQuery(name = "Category.findByItemID", query = "SELECT c FROM Category c WHERE c.categoryPK.itemID = :itemID"),
    @NamedQuery(name = "Category.findByKind", query = "SELECT c FROM Category c WHERE c.categoryPK.kind = :kind")})
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CategoryPK categoryPK;

    public Category() {
    }

    public Category(CategoryPK categoryPK) {
        this.categoryPK = categoryPK;
    }

    public Category(int itemID, String kind) {
        this.categoryPK = new CategoryPK(itemID, kind);
    }

    public CategoryPK getCategoryPK() {
        return categoryPK;
    }

    public void setCategoryPK(CategoryPK categoryPK) {
        this.categoryPK = categoryPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryPK != null ? categoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Category)) {
            return false;
        }
        Category other = (Category) object;
        if ((this.categoryPK == null && other.categoryPK != null) || (this.categoryPK != null && !this.categoryPK.equals(other.categoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Category[ categoryPK=" + categoryPK + " ]";
    }
    
}
