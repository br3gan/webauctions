/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author racoon
 */
@Entity
@Table(name = "ZombieItem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ZombieItem.findAll", query = "SELECT z FROM ZombieItem z"),
    @NamedQuery(name = "ZombieItem.findByZombieID", query = "SELECT z FROM ZombieItem z WHERE z.zombieID = :zombieID"),
    @NamedQuery(name = "ZombieItem.findByItemID", query = "SELECT z FROM ZombieItem z WHERE z.itemID = :itemID"),
    @NamedQuery(name = "ZombieItem.findByZombieName", query = "SELECT z FROM ZombieItem z WHERE z.zombieName = :zombieName"),
    @NamedQuery(name = "ZombieItem.findByZombieBuyPrice", query = "SELECT z FROM ZombieItem z WHERE z.zombieBuyPrice = :zombieBuyPrice"),
    @NamedQuery(name = "ZombieItem.findByZombieFirstBid", query = "SELECT z FROM ZombieItem z WHERE z.zombieFirstBid = :zombieFirstBid"),
    @NamedQuery(name = "ZombieItem.findByZombieBidsID", query = "SELECT z FROM ZombieItem z WHERE z.zombieBidsID = :zombieBidsID"),
    @NamedQuery(name = "ZombieItem.findByZombieLocationID", query = "SELECT z FROM ZombieItem z WHERE z.zombieLocationID = :zombieLocationID"),
    @NamedQuery(name = "ZombieItem.findByZombieStart", query = "SELECT z FROM ZombieItem z WHERE z.zombieStart = :zombieStart"),
    @NamedQuery(name = "ZombieItem.findByZombieEnd", query = "SELECT z FROM ZombieItem z WHERE z.zombieEnd = :zombieEnd"),
    @NamedQuery(name = "ZombieItem.findByZombieSellerID", query = "SELECT z FROM ZombieItem z WHERE z.zombieSellerID = :zombieSellerID"),
    @NamedQuery(name = "ZombieItem.findByZombieDescription", query = "SELECT z FROM ZombieItem z WHERE z.zombieDescription = :zombieDescription")})
public class ZombieItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "zombieID")
    private Integer zombieID;
    @Basic(optional = false)
    @Column(name = "itemID")
    private int itemID;
    @Basic(optional = false)
    @Column(name = "ZombieName")
    private String zombieName;
    @Basic(optional = false)
    @Column(name = "ZombieBuyPrice")
    private float zombieBuyPrice;
    @Basic(optional = false)
    @Column(name = "ZombieFirstBid")
    private float zombieFirstBid;
    @Basic(optional = false)
    @Column(name = "ZombieBidsID")
    private int zombieBidsID;
    @Basic(optional = false)
    @Column(name = "ZombieLocationID")
    private int zombieLocationID;
    @Basic(optional = false)
    @Column(name = "ZombieStart")
    @Temporal(TemporalType.TIMESTAMP)
    private Date zombieStart;
    @Basic(optional = false)
    @Column(name = "ZombieEnd")
    @Temporal(TemporalType.TIMESTAMP)
    private Date zombieEnd;
    @Basic(optional = false)
    @Column(name = "ZombieSellerID")
    private int zombieSellerID;
    @Column(name = "ZombieDescription")
    private String zombieDescription;

    public ZombieItem() {
    }

    public ZombieItem(Integer zombieID) {
        this.zombieID = zombieID;
    }

    public ZombieItem(Integer zombieID, int itemID, String zombieName, float zombieBuyPrice, float zombieFirstBid, int zombieBidsID, int zombieLocationID, Date zombieStart, Date zombieEnd, int zombieSellerID) {
        this.zombieID = zombieID;
        this.itemID = itemID;
        this.zombieName = zombieName;
        this.zombieBuyPrice = zombieBuyPrice;
        this.zombieFirstBid = zombieFirstBid;
        this.zombieBidsID = zombieBidsID;
        this.zombieLocationID = zombieLocationID;
        this.zombieStart = zombieStart;
        this.zombieEnd = zombieEnd;
        this.zombieSellerID = zombieSellerID;
    }

    public Integer getZombieID() {
        return zombieID;
    }

    public void setZombieID(Integer zombieID) {
        this.zombieID = zombieID;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getZombieName() {
        return zombieName;
    }

    public void setZombieName(String zombieName) {
        this.zombieName = zombieName;
    }

    public float getZombieBuyPrice() {
        return zombieBuyPrice;
    }

    public void setZombieBuyPrice(float zombieBuyPrice) {
        this.zombieBuyPrice = zombieBuyPrice;
    }

    public float getZombieFirstBid() {
        return zombieFirstBid;
    }

    public void setZombieFirstBid(float zombieFirstBid) {
        this.zombieFirstBid = zombieFirstBid;
    }

    public int getZombieBidsID() {
        return zombieBidsID;
    }

    public void setZombieBidsID(int zombieBidsID) {
        this.zombieBidsID = zombieBidsID;
    }

    public int getZombieLocationID() {
        return zombieLocationID;
    }

    public void setZombieLocationID(int zombieLocationID) {
        this.zombieLocationID = zombieLocationID;
    }

    public Date getZombieStart() {
        return zombieStart;
    }

    public void setZombieStart(Date zombieStart) {
        this.zombieStart = zombieStart;
    }

    public Date getZombieEnd() {
        return zombieEnd;
    }

    public void setZombieEnd(Date zombieEnd) {
        this.zombieEnd = zombieEnd;
    }

    public int getZombieSellerID() {
        return zombieSellerID;
    }

    public void setZombieSellerID(int zombieSellerID) {
        this.zombieSellerID = zombieSellerID;
    }

    public String getZombieDescription() {
        return zombieDescription;
    }

    public void setZombieDescription(String zombieDescription) {
        this.zombieDescription = zombieDescription;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (zombieID != null ? zombieID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ZombieItem)) {
            return false;
        }
        ZombieItem other = (ZombieItem) object;
        if ((this.zombieID == null && other.zombieID != null) || (this.zombieID != null && !this.zombieID.equals(other.zombieID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.ZombieItem[ zombieID=" + zombieID + " ]";
    }
    
}
