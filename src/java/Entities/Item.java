/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author racoon
 */
@Entity
@Table(name = "Item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i"),
    @NamedQuery(name = "Item.findById", query = "SELECT i FROM Item i WHERE i.id = :id"),
    @NamedQuery(name = "Item.findByName", query = "SELECT i FROM Item i WHERE i.name = :name"),
    @NamedQuery(name = "Item.findByCurrPrice", query = "SELECT i FROM Item i WHERE i.currPrice = :currPrice"),
    @NamedQuery(name = "Item.findByBuyPrice", query = "SELECT i FROM Item i WHERE i.buyPrice = :buyPrice"),
    @NamedQuery(name = "Item.findByFirstBid", query = "SELECT i FROM Item i WHERE i.firstBid = :firstBid"),
    @NamedQuery(name = "Item.findByNumberOfBids", query = "SELECT i FROM Item i WHERE i.numberOfBids = :numberOfBids"),
    @NamedQuery(name = "Item.findByBidsID", query = "SELECT i FROM Item i WHERE i.bidsID = :bidsID"),
    @NamedQuery(name = "Item.findByLocationID", query = "SELECT i FROM Item i WHERE i.locationID = :locationID"),
    @NamedQuery(name = "Item.findByStart", query = "SELECT i FROM Item i WHERE i.start = :start"),
    @NamedQuery(name = "Item.findByEnd", query = "SELECT i FROM Item i WHERE i.end = :end"),
    @NamedQuery(name = "Item.findBySellerID", query = "SELECT i FROM Item i WHERE i.sellerID = :sellerID"),
    @NamedQuery(name = "Item.findByItemDescription", query = "SELECT i FROM Item i WHERE i.itemDescription = :itemDescription")})
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @Column(name = "CurrPrice")
    private float currPrice;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BuyPrice")
    private Float buyPrice;
    @Basic(optional = false)
    @Column(name = "FirstBid")
    private float firstBid;
    @Basic(optional = false)
    @Column(name = "NumberOfBids")
    private int numberOfBids;
    @Basic(optional = false)
    @Column(name = "BidsID")
    private int bidsID;
    @Basic(optional = false)
    @Column(name = "LocationID")
    private int locationID;
    @Basic(optional = false)
    @Column(name = "Start")
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;
    @Basic(optional = false)
    @Column(name = "End")
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;
    @Basic(optional = false)
    @Column(name = "SellerID")
    private int sellerID;
    @Column(name = "ItemDescription")
    private String itemDescription;

    public Item() {
    }

    public Item(Integer id) {
        this.id = id;
    }

    public Item(Integer id, String name, float currPrice, float buyPrice, float firstBid, int numberOfBids, int bidsID, int locationID, Date start, Date end, int sellerID, String itemDesc) {
        this.id = id;
        this.name = name;
        this.currPrice = currPrice;
        this.buyPrice = buyPrice;
        this.firstBid = firstBid;
        this.numberOfBids = numberOfBids;
        this.bidsID = bidsID;
        this.locationID = locationID;
        this.start = start;
        this.end = end;
        this.sellerID = sellerID;
        this.itemDescription = itemDesc;
    }

//    public Item(int ID, String Name, float CurrPrice, float BuyPrice, float FirstBid, int NumberOfBids, int BidsID, int LocationID, Date Start, Date End, int SellerID, String ItemDesc) {
//        this.id = ID;
//        this.name = Name;
//        this.currPrice = CurrPrice;
//        this.buyPrice = BuyPrice;
//        this.firstBid = FirstBid;
//        this.numberOfBids = NumberOfBids;
//        this.bidsID = BidsID;
//        this.locationID = LocationID;
//        this.start = Start;
//        this.end = End;
//        this.sellerID = SellerID;
//        this.itemDescription = ItemDesc;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getCurrPrice() {
        return currPrice;
    }

    public void setCurrPrice(float currPrice) {
        this.currPrice = currPrice;
    }

    public Float getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Float buyPrice) {
        this.buyPrice = buyPrice;
    }

    public float getFirstBid() {
        return firstBid;
    }

    public void setFirstBid(float firstBid) {
        this.firstBid = firstBid;
    }

    public int getNumberOfBids() {
        return numberOfBids;
    }

    public void setNumberOfBids(int numberOfBids) {
        this.numberOfBids = numberOfBids;
    }

    public int getBidsID() {
        return bidsID;
    }

    public void setBidsID(int bidsID) {
        this.bidsID = bidsID;
    }

    public int getLocationID() {
        return locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getSellerID() {
        return sellerID;
    }

    public void setSellerID(int sellerID) {
        this.sellerID = sellerID;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Item)) {
            return false;
        }
        Item other = (Item) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Item[ id=" + id + " ]";
    }
    
    public void printItem() {
        System.out.println("ID="+ this.id);
        System.out.println("Name="+this.name);
        System.out.println("CurrPrice="+this.currPrice);
        System.out.println("BuyPrice="+this.buyPrice);
        System.out.println("FirstBid="+this.firstBid);
        System.out.println("NumberOfBids="+this.numberOfBids);
        System.out.println("BidsID="+this.bidsID);
        System.out.println("LocationID="+this.locationID);
        System.out.println("Start="+this.start);
        System.out.println("End="+this.end);
        System.out.println("SellerID="+this.sellerID);
        System.out.println("ItemDesc="+this.itemDescription);
    }
    
}
