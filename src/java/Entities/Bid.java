/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author racoon
 */
@Entity
@Table(name = "Bid")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bid.findAll", query = "SELECT b FROM Bid b"),
    @NamedQuery(name = "Bid.findByBidID", query = "SELECT b FROM Bid b WHERE b.bidID = :bidID"),
    @NamedQuery(name = "Bid.findByBidsID", query = "SELECT b FROM Bid b WHERE b.bidsID = :bidsID"),
    @NamedQuery(name = "Bid.findByUserID", query = "SELECT b FROM Bid b WHERE b.userID = :userID"),
    @NamedQuery(name = "Bid.findByBidTime", query = "SELECT b FROM Bid b WHERE b.bidTime = :bidTime"),
    @NamedQuery(name = "Bid.findByBidAmount", query = "SELECT b FROM Bid b WHERE b.bidAmount = :bidAmount")})
public class Bid implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bidID")
    private Integer bidID;
    @Basic(optional = false)
    @Column(name = "bidsID")
    private int bidsID;
    @Basic(optional = false)
    @Column(name = "userID")
    private int userID;
    @Basic(optional = false)
    @Column(name = "bidTime")
    @Temporal(TemporalType.TIME)
    private Date bidTime;
    @Basic(optional = false)
    @Column(name = "bidAmount")
    private float bidAmount;

    public Bid() {
    }

    public Bid(Integer bidID) {
        this.bidID = bidID;
    }

    public Bid(Integer bidID, int bidsID, int userID, Date bidTime, float bidAmount) {
        this.bidID = bidID;
        this.bidsID = bidsID;
        this.userID = userID;
        this.bidTime = bidTime;
        this.bidAmount = bidAmount;
    }

    public Integer getBidID() {
        return bidID;
    }

    public void setBidID(Integer bidID) {
        this.bidID = bidID;
    }

    public int getBidsID() {
        return bidsID;
    }

    public void setBidsID(int bidsID) {
        this.bidsID = bidsID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Date getBidTime() {
        return bidTime;
    }

    public void setBidTime(Date bidTime) {
        this.bidTime = bidTime;
    }

    public float getBidAmount() {
        return bidAmount;
    }

    public void setBidAmount(float bidAmount) {
        this.bidAmount = bidAmount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bidID != null ? bidID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bid)) {
            return false;
        }
        Bid other = (Bid) object;
        if ((this.bidID == null && other.bidID != null) || (this.bidID != null && !this.bidID.equals(other.bidID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Bid[ bidID=" + bidID + " ]";
    }

    public void printBid() {
        System.out.println(this.bidID);
        System.out.println(this.bidsID);
        System.out.println(this.userID);
        System.out.println(this.bidTime);
        System.out.println(this.bidAmount);
    }
    
}
