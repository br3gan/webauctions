/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author racoon
 */
@Embeddable
public class CategoryPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "ItemID")
    private int itemID;
    @Basic(optional = false)
    @Column(name = "Kind")
    private String kind;

    public CategoryPK() {
    }

    public CategoryPK(int itemID, String kind) {
        this.itemID = itemID;
        this.kind = kind;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) itemID;
        hash += (kind != null ? kind.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoryPK)) {
            return false;
        }
        CategoryPK other = (CategoryPK) object;
        if (this.itemID != other.itemID) {
            return false;
        }
        if ((this.kind == null && other.kind != null) || (this.kind != null && !this.kind.equals(other.kind))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.CategoryPK[ itemID=" + itemID + ", kind=" + kind + " ]";
    }
    
}
