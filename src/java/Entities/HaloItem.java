/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author racoon
 */
@Entity
@Table(name = "HaloItem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HaloItem.findAll", query = "SELECT h FROM HaloItem h"),
    @NamedQuery(name = "HaloItem.findByItemID", query = "SELECT h FROM HaloItem h WHERE h.itemID = :itemID"),
    @NamedQuery(name = "HaloItem.findByHaloName", query = "SELECT h FROM HaloItem h WHERE h.haloName = :haloName"),
    @NamedQuery(name = "HaloItem.findByHaloCurrPrice", query = "SELECT h FROM HaloItem h WHERE h.haloCurrPrice = :haloCurrPrice"),
    @NamedQuery(name = "HaloItem.findByHaloBuyPrice", query = "SELECT h FROM HaloItem h WHERE h.haloBuyPrice = :haloBuyPrice"),
    @NamedQuery(name = "HaloItem.findByHaloFirstBid", query = "SELECT h FROM HaloItem h WHERE h.haloFirstBid = :haloFirstBid"),
    @NamedQuery(name = "HaloItem.findByHaloNumberOfBids", query = "SELECT h FROM HaloItem h WHERE h.haloNumberOfBids = :haloNumberOfBids"),
    @NamedQuery(name = "HaloItem.findByHaloBidsID", query = "SELECT h FROM HaloItem h WHERE h.haloBidsID = :haloBidsID"),
    @NamedQuery(name = "HaloItem.findByHaloLocationID", query = "SELECT h FROM HaloItem h WHERE h.haloLocationID = :haloLocationID"),
    @NamedQuery(name = "HaloItem.findByHaloStart", query = "SELECT h FROM HaloItem h WHERE h.haloStart = :haloStart"),
    @NamedQuery(name = "HaloItem.findByHaloEnd", query = "SELECT h FROM HaloItem h WHERE h.haloEnd = :haloEnd"),
    @NamedQuery(name = "HaloItem.findByHaloSellerID", query = "SELECT h FROM HaloItem h WHERE h.haloSellerID = :haloSellerID"),
    @NamedQuery(name = "HaloItem.findByHaloDescription", query = "SELECT h FROM HaloItem h WHERE h.haloDescription = :haloDescription")})
public class HaloItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "itemID")
    private Integer itemID;
    @Basic(optional = false)
    @Column(name = "haloName")
    private String haloName;
    @Basic(optional = false)
    @Column(name = "haloCurrPrice")
    private float haloCurrPrice;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "haloBuyPrice")
    private Float haloBuyPrice;
    @Basic(optional = false)
    @Column(name = "haloFirstBid")
    private float haloFirstBid;
    @Basic(optional = false)
    @Column(name = "haloNumberOfBids")
    private int haloNumberOfBids;
    @Basic(optional = false)
    @Column(name = "haloBidsID")
    private int haloBidsID;
    @Basic(optional = false)
    @Column(name = "haloLocationID")
    private int haloLocationID;
    @Basic(optional = false)
    @Column(name = "haloStart")
    @Temporal(TemporalType.TIMESTAMP)
    private Date haloStart;
    @Basic(optional = false)
    @Column(name = "haloEnd")
    @Temporal(TemporalType.TIMESTAMP)
    private Date haloEnd;
    @Basic(optional = false)
    @Column(name = "haloSellerID")
    private int haloSellerID;
    @Column(name = "haloDescription")
    private String haloDescription;

    public HaloItem() {
    }

    public HaloItem(Integer itemID) {
        this.itemID = itemID;
    }

    public HaloItem(Integer itemID, String haloName, float haloCurrPrice, float haloFirstBid, int haloNumberOfBids, int haloBidsID, int haloLocationID, Date haloStart, Date haloEnd, int haloSellerID) {
        this.itemID = itemID;
        this.haloName = haloName;
        this.haloCurrPrice = haloCurrPrice;
        this.haloFirstBid = haloFirstBid;
        this.haloNumberOfBids = haloNumberOfBids;
        this.haloBidsID = haloBidsID;
        this.haloLocationID = haloLocationID;
        this.haloStart = haloStart;
        this.haloEnd = haloEnd;
        this.haloSellerID = haloSellerID;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public String getHaloName() {
        return haloName;
    }

    public void setHaloName(String haloName) {
        this.haloName = haloName;
    }

    public float getHaloCurrPrice() {
        return haloCurrPrice;
    }

    public void setHaloCurrPrice(float haloCurrPrice) {
        this.haloCurrPrice = haloCurrPrice;
    }

    public Float getHaloBuyPrice() {
        return haloBuyPrice;
    }

    public void setHaloBuyPrice(Float haloBuyPrice) {
        this.haloBuyPrice = haloBuyPrice;
    }

    public float getHaloFirstBid() {
        return haloFirstBid;
    }

    public void setHaloFirstBid(float haloFirstBid) {
        this.haloFirstBid = haloFirstBid;
    }

    public int getHaloNumberOfBids() {
        return haloNumberOfBids;
    }

    public void setHaloNumberOfBids(int haloNumberOfBids) {
        this.haloNumberOfBids = haloNumberOfBids;
    }

    public int getHaloBidsID() {
        return haloBidsID;
    }

    public void setHaloBidsID(int haloBidsID) {
        this.haloBidsID = haloBidsID;
    }

    public int getHaloLocationID() {
        return haloLocationID;
    }

    public void setHaloLocationID(int haloLocationID) {
        this.haloLocationID = haloLocationID;
    }

    public Date getHaloStart() {
        return haloStart;
    }

    public void setHaloStart(Date haloStart) {
        this.haloStart = haloStart;
    }

    public Date getHaloEnd() {
        return haloEnd;
    }

    public void setHaloEnd(Date haloEnd) {
        this.haloEnd = haloEnd;
    }

    public int getHaloSellerID() {
        return haloSellerID;
    }

    public void setHaloSellerID(int haloSellerID) {
        this.haloSellerID = haloSellerID;
    }

    public String getHaloDescription() {
        return haloDescription;
    }

    public void setHaloDescription(String haloDescription) {
        this.haloDescription = haloDescription;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemID != null ? itemID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HaloItem)) {
            return false;
        }
        HaloItem other = (HaloItem) object;
        if ((this.itemID == null && other.itemID != null) || (this.itemID != null && !this.itemID.equals(other.itemID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.HaloItem[ itemID=" + itemID + " ]";
    }
    
}
