<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 


  <div style=" letter-spacing: 5px; ">
      
    <h1 align = "center" ><em><strong>E - Auctions - Center</strong></em></h1>
    
  </div>
 
  <div style="text-align: right;"> 
        <c:choose>
            <c:when test="${loginedUser.username==null}">
                <br>Hello <b>Visitor</b>!
         </c:when>
         <c:otherwise>
                <br>Hello <b>${loginedUser.username}</b>!<br>
                <a href="${pageContext.request.contextPath}/logout"><font color="bisque">Logout</font></a>
         </c:otherwise>
         </c:choose>
    </div>