<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
 <head>
    <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/> 
    <meta charset="UTF-8">
    <title>Welcome - Login</title>
    <link href="myStyleSheet.css" rel="stylesheet" type="text/css">
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>

    
<div align="center"> 
    <h3>Welcome</h3>
 
    <p style="color: red;">${errorString}</p>
 
        <table border="0">
            <tr>
                <form method="POST" action="doLogin">
                <td align="right"><em><strong>Already A User: </strong></em></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right"><em><strong>Username: </strong></em></td>
                <td align="left"><input type="text" name="Username"/> </td>
            </tr>
            <tr>
                <td align="right"><em><strong>Password: </strong></em></td>
                <td align="left"><input type="password" name="Password"/> </td>
            </tr>
            <tr>
                <td align="right">Remember me </td>
                <td align="left"><input class="myCrs" type="checkbox" name="rememberMe" value= "Y" /> </td>
            </tr>
            <tr>
                <td  align="right"></td>
                <td  align="left">
                    <button class="myCrs" type="submit">Log in</button></td></form>
            </tr>
            <tr>
                <td align="right"><em><strong>New User: </strong></em></td>
                <td  align="left"><form method="POST" action="signup">
                        <button class="myCrs" type="submit">Sign up </button></form></td>
            </tr>
            <tr>
                <td align="right"><em><strong>Continue As Visitor</strong></em></td>
                <td  align="left"><form method="POST" action="bidder">
                        <button class="myCrs" type="submit">Let's go for a Tour</button></form></td>
            </tr>
        </table>
 
                </div>

    <jsp:include page="_footer.jsp"></jsp:include>
    
 
</body>
</html>
