<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
     <meta charset="UTF-8">
     <title>User Home Page</title>
  </head>
  <body>
  
     <jsp:include page="../_header.jsp"></jsp:include>
     <jsp:include page="../_menuUser.jsp"></jsp:include>
<div align="center" >
      <h3>Now choose your role:</h3><br>
      <form method="POST" action="bidder">
       <table border="0">
          <tr>
             <td colspan ="2">
                 <button type="submit" name="user" value="b">Enter as bidder</button>
                <a href="${pageContext.request.contextPath}/"></a>
             </td>
          </tr>
       </table>
    </form>
    <form method="POST" action="seller">
       <table border="0">
          <tr>
             <td colspan ="2">
                 <button type="submit" name="user" value="s">Enter as seller</button>
                <a href="${pageContext.request.contextPath}/"></a>
             </td>
          </tr>
       </table>
    </form>  
</div>             
             
      
     <jsp:include page="../_footer.jsp"></jsp:include>
 
  </body>
</html>
