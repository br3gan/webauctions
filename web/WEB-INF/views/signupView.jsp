<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/>    
    <meta charset="UTF-8">
    <title>Login</title>
 </head>
 <body>
     <script type="text/javascript">
  function checkForm(form)
  {
    if(form.Password.value !== form.rePassword.value) {
      
      alert("Error: The Passwords Are not the Same. Re- Enter them");
      form.Password.focus();
      return false;
    }
    return true;
  }

</script> 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
    
    <p align="center" class="error">${errorString}</p>
    
    <div align="center">
    <form method="POST" action="doSignup" onsubmit="return checkForm(this);">
       <table border="0">
        <tr>
            <td align="right"> <font><em><strong>Registration </strong></em></font></td>
            <td align="left"> <em><strong>Form</strong></em></td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>Username: </strong></em></font></td>
            <td><input type="text" name="Username"  required placeholder="Your Username"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>Password: </strong></em></font></td>
            <td><input type="password" name="Password" required placeholder="Your Password"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>Re-Enter Password: </strong></em></font></td>
            <td><input type="password" name="rePassword" required placeholder="Re-Enter your password"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>First Name: </strong></em></font></td>
            <td><input type="text" name="Firstname" required placeholder="Your First Name"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>Last Name: </strong></em></font></td>
            <td><input type="text" name="Lastname" required placeholder="Your Last Name"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>Mail: </strong></em></font></td>
            <td><input type="text" name="Mail" required placeholder="Your Mail"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>Telephone: </strong></em></font></td>
            <td><input type="text" name="Telephone" required placeholder="Your Telephone Number"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>Address: </strong></em></font></td>
            <td><input type="text" name="Address" required placeholder="Your Street-Name Number"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>Country: </strong></em></font></td>
            <td><input type="text" name="Country" required placeholder="Your Country"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>Post Code: </strong></em></font></td>
            <td><input type="text" name="PostCode" required placeholder="Your Post Code"/> </td>
        </tr>          
        <tr>
            <td align="right"> <font><em><strong>Social Security Number: </strong></em></font></td>
            <td><input type="text" name="SSN" required placeholder="Your SSN"/> </td>
        </tr>          
        <tr>
            <td></td>
            <td colspan ="2">
                <button class="myCrs" type="submit">Confirm</button>
             </td>
          </tr>
       </table>
    </form>
    </div>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>
