<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
    <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/>
    
    <title>Registration Confirm</title>
    <link rel="stylesheet" type="text/css" href="myStyleSheet.css">
    <meta charset="utf-8" http-equiv="Refresh" content="8;url='bidder'>
</head>
<body>
    <jsp:include page="_header.jsp"></jsp:include>

    <h1 align="center"><em><strong>Your registration form is completed Successfully!!!</strong></em>
</h1>
<br><br><br><br>

<h3 align="center"><em><strong>The administrators will contact you within 24-hours about the approval of your form.
	</strong></em></h3>
<p align="center">Thank you!!!<br>
	You will now continue as a visitor.<br>
	If you are not redirected automatically to the home page, please click 
        <a href="bidder"><font color="bisque">here</font></a>.</p>


<div class="logoPos">
<form>
	<a> 
		<img src="images/small_logo.png" id="radius-corners"/>
    </a>
</form>
</div>


</body>
</html>