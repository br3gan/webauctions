<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Items</title>
    </head>
    <body>
        
        <jsp:include page="../_header.jsp"></jsp:include>
        <jsp:include page="../_menuUser.jsp.jsp"></jsp:include>
        <h1>My Bids: </h1>
        
        
<div align="center">   
    <form action="?itemID='${SellingItems.id}'" method="post">
<c:if test="${SellingItems.size()>0}">
    <h3>Total Items on sale: ${SellingItems.size()}</h3>
    <table class="mytable">
        <tr>
            <th>Name</th>
            <th>Current Price</th>
            <th>Buy Price</th>
            <th>First Bid</th>
            <th>Number of Bids</th>
            <th>ID of Bid</th>
        
            <th>Categories</th>
            
            <th>Address - Post Code - Country</th>

            <th>Start of Bid</th>
            <th>End of Bid</th>
            <th>Description</th>
        </tr>
        <c:forEach var="i" begin="1" end="${SellingItems.size()}">
    <tr>
        <td>${SellingItems[i-1].name}</td>
        <td>${SellingItems[i-1].currPrice}</td>
        <td>${SellingItems[i-1].buyPrice}</td>
        <td>${SellingItems[i-1].firstBid}</td>
        <td>${SellingItems[i-1].numberOfBids}</td>
        <td>${SellingItems[i-1].bidsID}</td>
        
        <td>${SellingItemsCategories[i-1]}</td>
<%----%>        
        <td>${SellingItemsLocations[i-1]} - ${SellingItemsLocations[i-1].postCode} - ${SellingItemsLocations[i-1].country} </td>
<%----%>

        <td>${SellingItems[i-1].start}</td>
        <td>${SellingItems[i-1].end}</td>
        <td>${SellingItems[i-1].itemDescription}</td>
        <td class="lastCol"><button class="myCrs" type="submit" name="">Manage Bid</button></td>
    </tr>
    </c:forEach></table>


</c:if>
</div>

        <jsp:include page="../_footer.jsp"></jsp:include>
    </body>
</html>
