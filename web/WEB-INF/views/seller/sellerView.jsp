<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Seller page</title>
    </head>
    <body>
        <jsp:include page="../_header.jsp"></jsp:include>
        <jsp:include page="../_menuUser.jsp"></jsp:include>
        <h3 align="center">Pick your action</h3>
        <div align="center">
        <form method="POST" action="createBid">
            <table border="0">
                <tr>
                    <td colspan ="2">
                        <input class="myCrs" type="submit" value= "Create new bid" />
                        <a href="${pageContext.request.contextPath}/"></a>
                    </td>
                </tr>
            </table>
        </form>
        <form method="POST" action="viewBids">
            <table border="0">
                <tr>
                    <td colspan ="2">
                        <input class="myCrs" type="submit" value= "View my bids" />
                        <a href="${pageContext.request.contextPath}/"></a>
                    </td>
                </tr>
            </table>
        </form>
        <form method="POST" action="manageBids">
            <table border="0">
                <tr>
                    <td colspan ="2">
                        <input class="myCrs" type="submit" value= "Manage my bids" />
                        <a href="${pageContext.request.contextPath}/"></a>
                    </td>
                </tr>
            </table>
        </form>
        </div>
    <jsp:include page="../_footer.jsp"></jsp:include>
        
    </body>
</html>
