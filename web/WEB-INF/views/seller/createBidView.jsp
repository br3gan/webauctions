<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Item</title>
        <script src="/wp-includes/js/addInput.js" language="Javascript" type="text/javascript"></script>
    </head>
    <body>
        <script>
            function add_fields() {
                document.getElementById('category').innerHTML += '<span>Category: <input type="text"><small>(ft)</small></span>\r\n';
            }
        </script>1
        <script> 
            var counter = 1;
            var limit = 5;
            function addInput(divName){
                if (counter === limit)  {
                     alert("You have reached the limit of adding " + counter + " inputs");
                }
                else {
                     var newdiv = document.createElement('div');
                     newdiv.innerHTML = "Category " + (counter + 1) + ": <input type='text' name='Categories[]'/>";
                     document.getElementById(divName).appendChild(newdiv);
                     counter++;
                }
            }
        </script>        

        <jsp:include page="../_header.jsp"></jsp:include>
        <jsp:include page="../_menuUser.jsp"></jsp:include>
<div align="center">
        <h3>Insert Item info here</h3>
      
<form method="POST" action="doCreateBid">
    <div>
    <table border="0">
        <tr>
            <td align="right"> <font><em><strong>Name: </strong></em></font></td>
            <td><input type="text" name="Name" required/> </td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>First Bid: </strong></em></font></td>
            <td><input type="text" name="CurrPrice" required/> </td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>Buy Price: </strong></em></font></td>
            <td><input type="text" name="BuyPrice" required/> </td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>Start Time of Auction: </strong></em></font></td>
            <td><input type="datetime-local" name="Start" required/> </td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>End Time of Auction: </strong></em></font></td>
            <td><input type="datetime-local" name="End" required/> </td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>Description: </strong></em></font></td>
            <td><textarea name="ItemDescription" required></textarea> </td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>- </strong></em></font></td>
            <td align="left"> <font><em><strong>-</strong></em></font></td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>Location </strong></em></font></td>
            <td align="left"> <font><em><strong>Info:</strong></em></font></td>
        <tr>
        </tr>
            <td align="right"> <font><em><strong>Address: </strong></em></font></td>
            <td><input type="text" name="Address" required/> </td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>Post Code: </strong></em></font></td>
            <td><input type="text" name="Postcode" required/> </td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>Country: </strong></em></font></td>
            <td><input type="text" name="Country" required/> </td>
        </tr>
        <tr>
            <td align="right"> <font><em><strong>- </strong></em></font></td>
            <td align="left"> <font><em><strong>-</strong></em></font></td>
        </tr>
        </table>
        <table>
        <tr>
                
                    <form method="POST">
                     <div id="dynamicInput" align="center">
                         Category 1: <input type="text" name="Categories[]" required/>
                     </div>
                     <input class="myCrs" type="button" value="Add another category" onClick="addInput('dynamicInput');">
                </form>
                
    </tr>
        <tr>
            <td colspan ="2">
               <input class="myCrs" type="submit" value= "Submit" />
               </td>
        </tr>
    </table>
</form>
    </div>    
            
            
        <jsp:include page="../_footer.jsp"></jsp:include>
    </body>
</html>
