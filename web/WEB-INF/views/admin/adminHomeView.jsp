<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>


<html>
    <head>
        <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Home</title>
    </head>
    <body>
        
    <jsp:include page="../_header.jsp"></jsp:include>
    <jsp:include page="../_menuAdmin.jsp"></jsp:include>

        <h1 align="center">Admin Home Page</h1>

<c:if test="${Users.size()>0}">
    <div align="center">
    <h3>Users in System: ${Users.size()}</h3>

    <form method="post" action="viewUserInfo">            
        <table class="mytable">
            <tr class="firstrow">
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Mail</th>
                <th>Telephone</th>
    <!----> 
                <th>Address</th>
                <th>Post Code</th>
                <th>Country</th>
    <!---->

                <th>Seller Rating</th>                    
                <th>Bidder Rating</th>

                <th>Social Security Number</th>
            </tr>
            <c:forEach var="i" begin="1" end="${Users.size()}">
        <tr>
            <td>${Users[i-1].username}</td>
            <td>${Users[i-1].firstname}</td>
            <td>${Users[i-1].lastname}</td>
            <td>${Users[i-1].mail}</td>
            <td>${Users[i-1].telephone}</td>
    <%----%>
            <td>${Locations[i-1].address}</td>
            <td>${Locations[i-1].postCode}</td>
            <td>${Locations[i-1].country}</td>
    <%----%>

    
    <c:choose>
        <c:when test="${SellingRatings[i-1]<0}">
            <td>---</td>    
        </c:when>
        <c:otherwise>
            <td>${SellingRatings[i-1]}</td>
        </c:otherwise>
    </c:choose>
    
    <c:choose>
        <c:when test="${BiddingRatings[i-1]<0}">
        <td>---</td>    
        </c:when>
        <c:otherwise>
            <td>${BiddingRatings[i-1]}</td>
        </c:otherwise>
    </c:choose>

            <td>${Users[i-1].ssn}</td>
            <td class="lastCol"><button  class="myCrs" type="submit" name="userID" value="${Users[i-1].id}">Go to Profile</button>
        </tr>
            </c:forEach></table></form> </div>
</c:if>

<div align="center">
<c:if test="${UnconfirmedUsers.size()>0}">
<h3>Unconfirmed Users:  ${UnconfirmedUsers.size()}</h3>
    <table class="mytable">
        <tr class="firstrow">
            <th>Username</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Mail</th>

            <th>Address</th>
            <th>Post Code</th>
            <th>Country</th>

            <th>Telephone</th>
            <th>Social Security Number</th>
        </tr>
        <c:forEach var="i" begin="1" end="${UnconfirmedUsers.size()}">
            <tr>
            <td>${UnconfirmedUsers[i-1].username}</td>
            <td>${UnconfirmedUsers[i-1].firstname}</td>
            <td>${UnconfirmedUsers[i-1].lastname}</td>
            <td>${UnconfirmedUsers[i-1].mail}</td>

            <td>${UnconfirmedLocations[i-1].address}</td>
            <td>${UnconfirmedLocations[i-1].postCode}</td>
            <td>${UnconfirmedLocations[i-1].country}</td>

            <td>${UnconfirmedUsers[i-1].telephone}</td>
            <td>${UnconfirmedUsers[i-1].ssn}</td>
            <td><form action="acceptRegistration" method="post"><button class="myCrs" name="Username" value="${UnconfirmedUsers[i-1].username}"> Accept</button></form></td>
            <td><form action="deleteUnconfirmed" method="post"><button class="myCrs" name="Username" value="${UnconfirmedUsers[i-1].username}"> Reject</button></form></td>
        </tr>
            </c:forEach></table>
</c:if>
</div>
    <jsp:include page="../_footer.jsp"></jsp:include>

    </body>
</html>
