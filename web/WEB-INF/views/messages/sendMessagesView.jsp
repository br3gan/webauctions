<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/> 
        <link rel="stylesheet" href="myStyleSheet.css" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Send Message</title>
    </head>
    <body>
        <jsp:include page="../_header.jsp"></jsp:include>
        <jsp:include page="../_menuUser.jsp"></jsp:include>
        <jsp:include page="messList.jsp"></jsp:include>
        
        <div align="center">
        <form action="doSendMessages" method="POST">
            <table> 
                <tr>
                    <td><em><strong>Receiver Username: </strong></em></td>
                    <td><input type="text" name="ReceiverUsername" required/></td>
                </tr>
            </table>
            <textarea class="textArea" name="Message" 
                      placeholder="Type your message here..." required></textarea>
            <br>    
            <button class="myCrs" type="submit" id="myCrs">Send</button>
        </form>
        </div>


        <jsp:include page="../_footer.jsp"></jsp:include>

    </body>
</html>
