<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/> 
        <link rel="stylesheet" href="myStyleSheet.css" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Outgoing Messages</title>
    </head>
    <body>
        <jsp:include page="../_header.jsp"></jsp:include>
        <jsp:include page="../_menuUser.jsp"></jsp:include>
        <jsp:include page="messList.jsp"></jsp:include>
        
        <h4 align="center">Outgoing Messages</h4>
        <div align="right">
        <c:choose>
            <c:when test="${Messages.size()>0}">
                <table class="mytable">
                    <tr  class="firstrow">
                        <th class="firstCol">To</th>
                        <th>Message</th>
                        <th class="lastCol"></th>
                        <th class="lastCol"></th>
                    </tr>
                <c:forEach var="i" begin="1" end="${Messages.size()}">
                    <tr class="mytr">
                        <td>${Receivers[i-1]}</td>
                        <td>${Messages[i-1].im}</td>
                    </tr>
                </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <h3 align="center">There no Outgoing Messages.</h3>
            </c:otherwise>
        </c:choose>
    </div>
        
        <jsp:include page="../_footer.jsp"></jsp:include>
        
    </body>
</html>
