<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="myStyleSheet.css" rel="stylesheet" type="text/css">
        <title>Incoming Messages</title>
    </head>
    <body>
        <jsp:include page="../_header.jsp"></jsp:include>
        <jsp:include page="../_menuUser.jsp"></jsp:include>
        <jsp:include page="messList.jsp"></jsp:include>

        <h4 align="center">Incoming Messages</h4>
        <div align="right">
        <c:choose>
        <c:when test="${Messages.size()>'0'}">
            <table class="mytable">
                <tr class="firstrow">
                    <th class="firstCol">From</th>
                    <th>Message</th>
                    <th hidden=""></th>
                    <th hidden=""></th>
                </tr>                
                <c:forEach var="i" begin="1" end="${Messages.size()}" >
                    <c:choose>
                    <c:when test="${Messages[i-i].status==1}">
                        <tr  class="mytr" id="newMess">
                            <td>${Senders[i-1]}</td>
                            <td>${Messages[i-1].im}</td>
                            <td class="newTag">New</td>
                            <td class="lastCol"><form action="doCheckIncoming" method="post">
                                    <button   class="myCrs" type="submit" name="MessageID" value="${Messages[i-1].id}">
                                        Read</button></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr class="mytr">
                            <td>${Senders[i-1]}</td>
                            <td>${Messages[i-1].im}</td>
                            <td class="lastCol"></td>
                            <td class="lastCol"></td>
                    </tr>
                    </c:otherwise>
                    </c:choose>
                </c:forEach>                
            </table>
        </c:when>
        <c:otherwise>
            <h3 align="center">There no Incoming Messages</h3>
        </c:otherwise>
        </c:choose>
        </div>
        <jsp:include page="../_footer.jsp"></jsp:include>

    </body>
</html>
