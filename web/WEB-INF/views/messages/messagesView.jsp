<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Messages</title>
    </head>
    <body>
        <jsp:include page="../_header.jsp"></jsp:include>
        <jsp:include page="../_menuUser.jsp"></jsp:include>
        <div align="center">
            Choose which Message - category you want to see.
            <jsp:include page="messList.jsp"></jsp:include>
        </div>
        <jsp:include page="../_footer.jsp"></jsp:include>
    </body>
</html>

