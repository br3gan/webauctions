<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
     <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/>
    <meta charset="UTF-8">
    <title>Item Info</title>
 </head>
 <body>
 
    <jsp:include page="../_header.jsp"></jsp:include>
    <jsp:include page="../_menuUser.jsp"></jsp:include>
 
        <h4 align="center"><font><em><strong>Item Information <br></strong></em></font></h4>

    <div align="center">
<form method="post" action="placeBid?ItemID=${Item.id}">
    <table>
    <tr>
            <td align="right"> <font><em><strong>Name: </strong></em></font></td>
            <td align="left">${Item.name}</td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Current Price: </strong></em></font></td>
            <td align="left">${Item.currPrice}</td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Buy Price: </strong></em></font></td>
            <td align="left">${Item.buyPrice}</td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>- </strong></em></font></td>
            <td align="left"> <font><em><strong>-</strong></em></font></td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Seller </strong></em></font></td>
            <td align="left"> <font><em><strong>Info: </strong></em></font></td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Username: </strong></em></font></td>
            <td align="left">${SellerUserame}</td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Rating: </strong></em></font></td>
    <c:choose>
        <c:when test="${Rating<0}">
            <td>Not rated yet</td>
        </c:when>
        <c:otherwise>
            <td align="left">${Rating}</td>
        </c:otherwise>
    </c:choose>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>- </strong></em></font></td>
            <td align="left"> <font><em><strong>-</strong></em></font></td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Location </strong></em></font></td>
            <td align="left"> <font><em><strong>Info: </strong></em></font></td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Address: </strong></em></font></td>
            <td align="left">${Location.address}</td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Post Code: </strong></em></font></td>
            <td align="left">${Location.postCode}</td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Country: </strong></em></font></td>
            <td align="left">${Location.country}</td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>- </strong></em></font></td>
            <td align="left"> <font><em><strong>-</strong></em></font></td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Bid Start Time: </strong></em></font></td>
            <td align="left">${Item.start}</td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Bid End Type: </strong></em></font></td>
            <td align="left">${Item.end}</td>
    </tr>
    <tr>
            <td align="right"> <font><em><strong>Description: </strong></em></font></td>
            <td align="left">${Item.itemDescription}</td>
    </tr>
    <tr>
        <td align="right"> <font><em><strong>Amount to Bid: </strong></em></font></td>
        <td align="left"><input type="text" id="BidAmount" name="BidAmount" placeholder="Your bid" required></td>
    </tr>
    <tr>
        <td></td>
        <td align="left"><button class="myCrs" type="submit" >Bid me!</button></td>
    </tr>
    </table>
    </form>
</div>

<jsp:include page="../_footer.jsp"></jsp:include>
 
 </body>
</html>