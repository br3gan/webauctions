<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
     <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/>
    <meta charset="UTF-8">
    <title>Product List</title>
    <link rel="stylesheet" href="myStyleSheet.css" type="text/css">
 </head>
 <body>
 
    <jsp:include page="../_header.jsp"></jsp:include>
    <jsp:include page="../_menuUser.jsp"></jsp:include>
 
    <h3>All Items:</h3>
    <div align="center">
    <form method="post" action="itemInfo">
      <table class="mytable" >
        <tr class="firstrow">
            <th>Name</th>
            <th>Current Price</th>
            <th>Buy Price</th>
            <th>End</th>
            <th>Categories</th>
            <th>Address - Post Code - Country</th>
            <th hidden ></th>
        </tr>
              
        <c:forEach var="i" begin="1" end="${Items.size()}">
            <tr class="mytr">
                <td>${Items[i-1].name}</td>
                <td>${Items[i-1].currPrice}</td>
                <td>${Items[i-1].buyPrice}</td>
                <td>${Items[i-1].end}</td>
                <td>
                    <c:forEach  var="Category" items="${ListOfItemCategories[i-1]}">
                        ${Category}<br>
                    </c:forEach>
                </td>
                <td>${Locations[i-1].address} - ${Locations[i-1].postCode} - ${Locations[i-1].country}</td>
                <td class="lastCol"><button class="myCrs" name="ItemID" value="${Items[i-1].id}">More...</button></td>
            </tr>
        </c:forEach>
    </table>
 </form>
    </div>
    <jsp:include page="../_footer.jsp"></jsp:include>
 
 </body>
</html>