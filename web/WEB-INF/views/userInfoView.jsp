<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <link rel="shortcut icon" href="images/online_auctions.jpg" type="image/x-icon"/>
    <meta charset="UTF-8">
    <title>User Info</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menuUser.jsp"></jsp:include>
     
    <div align="center">

<table>
<tr>
    <td><h1 align="right"><font><em><strong>User </strong></em></h1></td>
    <td><h1 align="left"><font><em><strong>Profile</strong></em></h1></td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Username: </strong></em></font></td>
        <td align="left">${User.username}</td>
</tr>
<tr>
	<td align="right"> <font><em><strong>First Name: </strong></em></font></td>
	<td align="left">${User.firstname}</td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Last Name: </strong></em></font></td>
	<td align="left">${User.lastname}</td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Social Security Number: </strong></em></font></td>
	<td align="left">${User.ssn}</td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Seller Rating: </strong></em></font></td>
    <c:choose>
        <c:when test="${SellingRating<0}">
            <td>Not rated yet</td>
        </c:when>
        <c:otherwise>
            <td align="left">${SellingRating}</td>
        </c:otherwise>
    </c:choose>

<tr>
        <td align="right"> <font><em><strong>Bidder Rating: </strong></em></font></td>
    <c:choose>
        <c:when test="${BiddingRating<0}">
            <td>Not rated yet</td>
        </c:when>
        <c:otherwise>
            <td align="left">${BiddingRating}</td>
        </c:otherwise>
    </c:choose>
</tr>
<tr>
	<td align="right"> <font><em><strong>- </strong></em></font></td>
	<td align="left"> <font><em><strong>-</strong></em></font></td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Contact </strong></em></font></td>
	<td align="left"> <font><em><strong>Info: </strong></em></font></td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Phone Number: </strong></em></font></td>
	<td align="left">${User.telephone}</td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Contact email: </strong></em></font></td>
	<td align="left">${User.mail}</td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Address: </strong></em></font></td>
	<td align="left">${Location.address}</td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Post Code: </strong></em></font></td>
	<td align="left">${Location.postCode}</td>
</tr>
<tr>
	<td align="right"> <font><em><strong>Country: </strong></em></font></td>
	<td align="left">${Location.country}</td>
</tr>
<tr>
	<td align="right"> <font><em><strong>- </strong></em></font></td>
	<td align="left"> <font><em><strong>-</strong></em></font></td>
</tr>
</table>
</div>
        
<div align="center">   
<c:choose>
<c:when test="${SellingItems.size()>0}">
    <h3>Total Items on sale by ${User.username}: ${SellingItems.size()}</h3>
    <table class="mytable">
        <tr class="firstrow">
            <th>Name</th>
            <th>Current Price</th>
            <th>Buy Price</th>
            <th>First Bid</th>
            <th>Number of Bids</th>
            <th>ID of Bid</th>
        
            <th>Categories</th>
            
            <th>Address - Post Code - Country</th>

            <th>Start of Bid</th>
            <th>End of Bid</th>
            <th>Description</th>
        </tr>
        <c:forEach var="i" begin="1" end="${SellingItems.size()}">
    <tr class="mytr">
        <td>${SellingItems[i-1].name}</td>
        <td>${SellingItems[i-1].currPrice}</td>
        <td>${SellingItems[i-1].buyPrice}</td>
        <td>${SellingItems[i-1].firstBid}</td>
        <td>${SellingItems[i-1].numberOfBids}</td>
        <td>${SellingItems[i-1].bidsID}</td>
        <td><c:forEach  var="Category" items="${SellingItemsCategories[i-1]}">
                        ${Category}<br>
            </c:forEach></td>

        <td>${SellingItemsLocations[i-1].address} - ${SellingItemsLocations[i-1].postCode} - ${SellingItemsLocations[i-1].country} </td>
        <td>${SellingItems[i-1].start}</td>
        <td>${SellingItems[i-1].end}</td>
        <td>${SellingItems[i-1].itemDescription}</td>
    </tr>
    </c:forEach></table>


</c:when>
<c:otherwise>
    <h3>No Items on sale by ${User.username}</h3>
</c:otherwise>
</c:choose>    
</div>


<div align="center">   
<c:choose>
<c:when test="${BiddingItems.size()>0}">
    <h3>Total Items bidden by ${User.username}: ${BiddingItems.size()}</h3>
    <table class="mytable">
        <tr class="firstrow">
            <th>Name</th>
            <th>ID of Bid</th>
            <th>Address - Post code - Country</th>

            <th>Time of Bidding</th>
            <th>Amount of Bid</th>
            <th>Item Description</th>
        </tr>
        <c:forEach var="i" begin="1" end="${BiddingItems.size()}">
            <tr class="mytr">
        <td>${BiddingItems[i-1].name}</td>
        <td>${BiddingItems[i-1].bidsID}</td>
        <td>${BiddingItemsLocations[i-1].address} - ${BiddingItemsLocations[i-1].postCode} - ${BiddingItemsLocations[i-1].country} </td>


        <td>${BiddingItemsBidInfo[i-1].bidTime}</td>
        <td>${BiddingItemsBidInfo[i-1].bidAmount}</td>
        <td>${BiddingItems[i-1].itemDescription}</td>
    </tr>
    </c:forEach></table>


</c:when>
<c:otherwise>
    <h3>No Items bidden by ${User.username}</h3>
</c:otherwise>
</c:choose>    
</div>


    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>